import Axios, { AxiosError } from "axios";
import { ApiBaseUrl } from "../configuration";
import { IFetchResponse } from "../types/IFetchResponse";

export abstract class BaseService {
    protected static axios = Axios.create({
        baseURL: ApiBaseUrl,
        headers: {
            'Content-Type': 'application/json'
        }
    });

    static async getAll<TEntity>(apiEndPoint: string): Promise<IFetchResponse<TEntity[]>> {
        try {
            let response = await this.axios.get<TEntity[]>(apiEndPoint);
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500
            }
        }
    }

    static async get<TEntity>(apiEndPoint: string, id: string): Promise<IFetchResponse<TEntity>> {
        let url = ApiBaseUrl + apiEndPoint + id;

        try {
            let response = await this.axios.get<TEntity>(url);
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500
            }
        }
    }

    static async create<TEntityAdd>(apiEndPoint: string, entity: TEntityAdd){
        const url = ApiBaseUrl + apiEndPoint;
        try {
            const response = await this.axios.post(url, JSON.stringify(entity));
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500
            };
        }
    }

    static async edit<TEntity>(apiEndPoint: string, id: string, entity: TEntity) {
        const url = ApiBaseUrl + apiEndPoint + id;
        try {
            const response = await this.axios.put(url, JSON.stringify(entity));
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500
            };
        }
    }

    static async delete(apiEndPoint: string, id: string){
        const url = ApiBaseUrl + apiEndPoint + id;
        try {
            const response = await this.axios.delete(url);
            return {
                ok: response.status <= 299,
                statusCode: response.status,
                data: response.data
            }
        } catch (error) {
            let err = error as AxiosError;
            return {
                ok: false,
                statusCode: err.response?.status ?? 500
            };
        }
    }
}