import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { Navigate, Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import RootIndex from './containers/root/RootIndex';
import ShipmentCreate from './containers/shipments/ShipmentCreate';
import ShipmentDetails from './containers/shipments/ShipmentDetails';
import ShipmentEdit from './containers/shipments/ShipmentEdit';
import Page404 from './containers/Page404';
import ParcelsBagsCreate from './containers/parcels-bags/ParcelsBagsCreate';
import ParcelsBagsEdit from './containers/parcels-bags/ParcelsBagsEdit';
import ParcelEdit from './containers/parcels/ParcelEdit';
import ParcelCreate from './containers/parcels/ParcelCreate';
import LettersBagsCreate from './containers/letters-bags/LettersBagsCreate';
import LettersBagsEdit from './containers/letters-bags/LettersBagsEdit';
import LettersBagsDetails from './containers/letters-bags/LettersBagsDetails';
import ParcelsBagsDetails from './containers/parcels-bags/ParcelsBagsDetails';
import ParcelDetails from './containers/parcels/ParcelDetails';
import ParcelsBagsIndex from './containers/parcels-bags/ParcelsBagsIndex';
import LettersBagsIndex from './containers/letters-bags/LettersBagsIndex';
import ParcelIndex from './containers/parcels/ParcelIndex';

const App = () => {

  return (
    <>
      <Header />
      <div className="container">
        <main role="main" className="pb-3">
          <Routes>
            <Route path="/" element={<RootIndex />} />

            <Route path="/shipments/create" element={<ShipmentCreate />} />
            <Route path="/shipments/edit/:id" element={<ShipmentEdit />} />
            <Route path="/shipments/:id" element={<ShipmentDetails />} />

            <Route path="/parcelsBags" element={<ParcelsBagsIndex/>}/>
            <Route path="/parcelsBags/create/:id" element={<ParcelsBagsCreate/>}/>
            <Route path="/parcelsBags/create" element={<ParcelsBagsCreate/>}/>
            <Route path="/parcelsBags/edit/:id" element={<ParcelsBagsEdit/>}/>
            <Route path="/parcelsBags/:id" element={<ParcelsBagsDetails/>}/>

            <Route path="/lettersBags" element={<LettersBagsIndex/>}/>
            <Route path="/lettersBags/create/:id" element={<LettersBagsCreate/>}/>
            <Route path="/lettersBags/create" element={<LettersBagsCreate/>}/>
            <Route path="/lettersBags/edit/:id" element={<LettersBagsEdit/>}/>
            <Route path="/lettersBags/:id" element={<LettersBagsDetails/>}/>

            <Route path="/parcels" element={<ParcelIndex/>}/>
            <Route path="/parcels/create/:id" element={<ParcelCreate/>}/>
            <Route path="/parcels/create" element={<ParcelCreate/>}/>
            <Route path="/parcels/edit/:id" element={<ParcelEdit/>}/>
            <Route path="/parcels/:id" element={<ParcelDetails/>}/>

            <Route path="*" element={<Page404 />} />
          </Routes>
        </main>
      </div>
      <Footer />
    </>
  );
}

export default App;
