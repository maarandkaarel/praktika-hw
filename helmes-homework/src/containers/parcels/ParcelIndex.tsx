import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { IParcel } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import Alert, { EAlertClass } from "../../components/Alert";

const RowDisplay = (props: { parcel: IParcel }) => {
    return (
        <>
            <td>
                <SubTitle2 subTitle={"Parcel Number"} />
                <RowItem item={props.parcel.parcelNumber} />

                <SubTitle2 subTitle={"Parcels Bag Number"} />
                <RowItem item={props.parcel.bagNumber} />

                <SubTitle2 subTitle={"Destination Country"} />
                <RowItem item={props.parcel.destinationCountry} />

                <SubTitle2 subTitle={"Recipients Name"} />
                <RowItem item={props.parcel.recipientName} />

                <SubTitle2 subTitle={"Weight"} />
                <RowItem item={props.parcel.weight} />

                <SubTitle2 subTitle={"Price"} />
                <RowItem item={props.parcel.price} />
            </td>
        </>
    )
}

const ParcelIndex = () => {

    const [parcels, setParcels] = useState([] as IParcel[]);
    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessge] = useState('');

    const loadData = async () => {
        let response = await BaseService.getAll<IParcel>('/Parcels');
        let parcelsBagsResponse = await BaseService.getAll<IParcelsBag>('/ParcelsBags')
        if (response.ok && response.data && parcelsBagsResponse.ok && parcelsBagsResponse.data) {
            setParcels(response.data);
            setParcelsBags(parcelsBagsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <div>
                <Title title={"Parcels"} />
                <Link to={"/parcels/create"}>Add new Parcel</Link>
            </div>

            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

            <table className="table">
                <tbody>
                    <>
                        {parcels.map(parcel =>
                            <>
                                <tr className="form-control hover-element mb-3" onClick={e => navigate("/parcels/" + parcel.parcelNumber)}>
                                    <RowDisplay parcel={parcel} key={parcel.parcelNumber} />
                                </tr>
                            </>
                        )}
                    </>
                </tbody>
            </table>
            <Loader {...pageStatus} />
        </>
    )
}

export default ParcelIndex;