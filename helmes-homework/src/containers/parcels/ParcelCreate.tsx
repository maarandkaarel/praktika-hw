import { useEffect, useState } from "react";
import { EAlertClass } from "../../components/Alert";
import { useNavigate, useParams } from "react-router-dom";
import Title from "../../components/Title";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import Alert from "../../components/Alert";
import 'react-datepicker/dist/react-datepicker.css';
import { IParcelAdd } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { IRouteId } from "../../types/IRouteId";


const ParcelCreate = () => {
    const { id } = useParams() as unknown as IRouteId;

    const [parcel, setParcel] = useState({} as IParcelAdd);
    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessage] = useState('');
    const navigate = useNavigate();

    const createClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        console.log(parcel)

        if (parcel.recipientName === undefined || parcel.destinationCountry === undefined || parcel.weight === undefined || parcel.price === undefined || parcel.bagNumber === undefined || parcel.bagNumber === "") {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: 666 });
            setAlertMessage('Empty fields!');
        }
        else {
            let response = await BaseService.create<IParcelAdd>("Parcels", parcel);
            if (response.ok) {
                navigate('/parcelsBags/' + parcel.bagNumber)
            } else {
                setAlertMessage('Parcel Creation unsuccessful: ' + response.statusCode)
            }
        }
    }

    const loadData = async () => {
        let parcelsBagsResponse = await BaseService.getAll<IParcelsBag>('/ParcelsBags');
        if (parcelsBagsResponse.ok && parcelsBagsResponse.data) {
            setParcelsBags(parcelsBagsResponse.data);
            if (id !== undefined) {
                setParcel({ ...parcel, bagNumber: id })
            }
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: parcelsBagsResponse.statusCode });
        }
    }


    function toFixedTrunc(x: number, n: number) {
        const v = (typeof x === 'string' ? x : x.toString()).split('.');
        if (n <= 0) {
            return parseFloat(v[0]);
        }
        let f = v[1] || '';
        if (f.length > n) {
            return parseFloat(`${v[0]}.${f.substr(0, n)}`);
        }
        while (f.length < n) f += '0';

        return parseFloat(`${v[0]}.${f}`);
    }


    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <Title title={"Create Parcel"} />
            <form onSubmit={(e) => createClicked(e.nativeEvent)}>
                <div className="row">
                    <div className="col-md-6">
                        <section>

                            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

                            <div className="form-group mb-2">
                                <label htmlFor="Parcel_RecipientsName">Recipients Name</label>
                                <input value={parcel.recipientName}
                                    onChange={e => setParcel({ ...parcel, recipientName: e.target.value })}
                                    className="form-control" type="string" id="Parcel_RecipientsName" name="Input.RecipientsName" placeholder="Recipients Name" />
                            </div>

                            {/* Ideally would create a dropdown with all the countries and their Alpha 2 Codes */}
                            <div className="form-group mb-2">
                                <label htmlFor="Parcel_Destination">Destination Country (Alpha 2 Code)</label>
                                <input value={parcel.destinationCountry}
                                    onChange={e => setParcel({ ...parcel, destinationCountry: e.target.value })}
                                    className="form-control" type="string" id="Parcel_Destination" name="Input.Destination"
                                    placeholder="Insert Destination Country"
                                    maxLength={2} />
                            </div>

                            <div className="form-group mb-2">
                                <label htmlFor="Parcel_Weight">Weight</label>
                                <input value={parcel.weight}
                                    onChange={e => setParcel({ ...parcel, weight: toFixedTrunc(e.target.valueAsNumber, 3) })}
                                    className="form-control" type="number" id="Parcel_Weight" name="Input.Weight" placeholder="Weight of the parcel"
                                    min="0" step="0.001"
                                />
                            </div>

                            <div className="form-group mb-2">
                                <label htmlFor="Parcel_Price">Price €</label>
                                <input value={parcel.price}
                                    onChange={e => setParcel({ ...parcel, price: toFixedTrunc(e.target.valueAsNumber, 2) })}
                                    className="form-control" type="number" id="Parcel_Price" name="Input.Price" placeholder="Price of the parcel"
                                    min="0" step="0.01"
                                />
                            </div>

                            <div className="form-group mb-2">
                                <label className="control-label" htmlFor="Parcel_ParcelsBagNumber">Parcels Bag</label>
                                <select className="form-control" id="Parcel_ParcelsBagNumber" name="Parcel.ParcelsBagNumber"
                                    onChange={parcelsBag => setParcel({ ...parcel, bagNumber: parcelsBag.target.value })}
                                    value={parcel.bagNumber}>
                                    <option value="">--- Please select ---</option>
                                    {parcelsBags.map(value => <option key={value.bagNumber}>{value.bagNumber}</option>)}
                                </select>
                            </div>

                        </section>
                    </div>
                </div>
            </form>
            <div className="form-group mb-2 mt-2">
                <button onClick={(e) => { createClicked(e.nativeEvent) }} type="submit" className="btn btn-primary">Create</button>
            </div>
        </>
    )
}

export default ParcelCreate;