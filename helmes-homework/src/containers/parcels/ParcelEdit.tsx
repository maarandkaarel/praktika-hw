import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import Title from "../../components/Title";
import { IParcel } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import Alert, { EAlertClass } from "../../components/Alert";

const ParcelEdit = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [parcel, setParcel] = useState({} as IParcel);
    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const deleteClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        setSuccessMessage('');

        let response = await BaseService.delete('Parcels/', id);
        if (response.ok) {
            navigate('/parcelsBags/' + parcel.bagNumber)
        } else {
            setAlertMessage("Parcel Delete UNSUCCESSFUL!");
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    const editClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        setSuccessMessage('');
        if (parcel.bagNumber === undefined || parcel.bagNumber === '' || parcel.parcelNumber === undefined || parcel.parcelNumber === '' ||
            parcel.destinationCountry === undefined || parcel.destinationCountry === '' || parcel.recipientName === undefined || parcel.recipientName === '' ||
            parcel.weight === undefined || parcel.price === undefined) {
            setAlertMessage("Empty fields!");
        } else {
            let response = await BaseService.edit<IParcel>('parcels/', id, parcel);
            if (response.ok) {
                navigate('/parcelsBags/' + parcel.bagNumber)
            } else {
                setAlertMessage('Parcel edit UNSUCCESSFUL!');
            }
        }
    }
    
    function toFixedTrunc(x: number, n: number) {
        const v = (typeof x === 'string' ? x : x.toString()).split('.');
        if (n <= 0) {
            return parseFloat(v[0]);
        }
        let f = v[1] || '';
        if (f.length > n) {
            return parseFloat(`${v[0]}.${f.substr(0, n)}`);
        }
        while (f.length < n) f += '0';

        return parseFloat(`${v[0]}.${f}`);
    }

    const loadData = async () => {
        let response = await BaseService.get<IParcel>('Parcels/', id);
        let parcelsBagsResponse = await BaseService.getAll<IParcelsBag>('/ParcelsBags')
        if (response.ok && response.data && parcelsBagsResponse.ok && parcelsBagsResponse.data) {
            setParcel(response.data);
            setParcelsBags(parcelsBagsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            {pageStatus.pageStatus == EPageStatus.OK ?
                <>
                    <div>
                        <Title title={"Edit Parcel " + parcel.parcelNumber} />
                    </div>

                    <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />
                    <Alert show={successMessage !== ''} message={successMessage} alertClass={EAlertClass.Success} />

                    <div className="float-end">
                        <DeleteRounded onClick={(e) => deleteClicked(e.nativeEvent)} className="hover-element" fontSize="large" htmlColor="red" />
                    </div>

                    <table className="table">
                        <tbody>
                            <>
                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="ParcelNumber">Parcel Number</label>
                                    <input className="form-control" disabled={true} type="text" id="ParcelNumber" maxLength={8} name="ParcelNumber"
                                        value={parcel.parcelNumber} onChange={parcelNr => setParcel({ ...parcel, parcelNumber: parcelNr.target.value })} />
                                </tr>

                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="Parcel_RecipientsName">Recipients Name</label>
                                    <input className="form-control" type="text" id="Parcel_RecipientsName" maxLength={128} name="Parcel.RecipientsName" placeholder="Recipients Name"
                                        value={parcel.recipientName} onChange={name => setParcel({ ...parcel, recipientName: name.target.value })} />
                                </tr>

                                <tr className="form-group mb-2">
                                    <label htmlFor="Parcel_Destination">Destination Country (Alpha 2 Code)</label>
                                    <input value={parcel.destinationCountry}
                                        onChange={e => setParcel({ ...parcel, destinationCountry: e.target.value })}
                                        className="form-control" type="string" id="Parcel_Destination" name="Input.Destination"
                                        placeholder="Insert Destination Country"
                                        maxLength={2} />
                                </tr>

                                <tr className="form-group mb-2">
                                    <label htmlFor="Parcel_Weight">Weight</label>
                                    <input value={parcel.weight}
                                        onChange={e => setParcel({ ...parcel, weight: toFixedTrunc(e.target.valueAsNumber, 3) })}
                                        className="form-control" type="number" id="Parcel_Weight" name="Input.Weight" placeholder="Weight of the parcel" 
                                        min="0" step="0.001"/>
                                </tr>

                                <tr className="form-group mb-2">
                                    <label htmlFor="Parcel_Price">Price €</label>
                                    <input value={parcel.price}
                                        onChange={e => setParcel({ ...parcel, price: toFixedTrunc(e.target.valueAsNumber, 2) })}
                                        className="form-control" type="number" id="Parcel_Price" name="Input.Price" placeholder="Price of the parcel"
                                        min="0" step="0.01"
                                    />
                                </tr>

                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="Parcel_ParcelsBagNumber">Parcels Bag</label>
                                    <select className="form-control" id="Parcel_ParcelsBagNumber" name="Parcel.ParcelsBagNumber"
                                        onChange={parcelsBag => setParcel({ ...parcel, bagNumber: parcelsBag.target.value })}
                                        value={parcel.bagNumber}>
                                        <option value="">--- Please select ---</option>
                                        {parcelsBags.map(value => <option key={value.bagNumber}>{value.bagNumber}</option>)}
                                    </select>
                                </tr>

                                <tr className="form-group mb-2 mt-2">
                                    <td>
                                        <button onClick={(e) => editClicked(e.nativeEvent)} type="submit" className="btn btn-primary">Edit</button>
                                    </td>
                                </tr>
                            </>
                        </tbody>
                    </table>
                </>
                :
                <Loader {...pageStatus} />}
        </>
    )
}

export default ParcelEdit;