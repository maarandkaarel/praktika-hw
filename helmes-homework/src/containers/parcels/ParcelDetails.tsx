import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { IParcel } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import EditRounded from '@material-ui/icons/EditRounded';
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import Alert, { EAlertClass } from "../../components/Alert";

const RowDisplay = (props: { parcel: IParcel }) => {
    return (
        <>
            <td>
                <SubTitle2 subTitle={"Parcel Number"} />
                <RowItem item={props.parcel.parcelNumber} />

                <SubTitle2 subTitle={"Parcels Bag Number"} />
                <RowItem item={props.parcel.bagNumber} />

                <SubTitle2 subTitle={"Destination Country"} />
                <RowItem item={props.parcel.destinationCountry} />

                <SubTitle2 subTitle={"Recipients Name"} />
                <RowItem item={props.parcel.recipientName} />

                <SubTitle2 subTitle={"Weight"} />
                <RowItem item={props.parcel.weight} />

                <SubTitle2 subTitle={"Price"} />
                <RowItem item={props.parcel.price} />
            </td>
        </>
    )
}


const ParcelDetails = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [parcel, setParcel] = useState({} as IParcel);
    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessge] = useState('');

    const deleteClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessge('');

        let response = await BaseService.delete('Parcels/', id);
        if (response.ok) {
            navigate('/parcelsBags/' + parcel.bagNumber)
        } else {
            setAlertMessge("Parcel Delete UNSUCCESSFUL!");
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    const loadData = async () => {
        let response = await BaseService.get<IParcel>('Parcels/', id);
        let parcelsBagsResponse = await BaseService.getAll<IParcelsBag>('/ParcelsBags')
        if (response.ok && response.data && parcelsBagsResponse.ok && parcelsBagsResponse.data) {
            setParcel(response.data);
            setParcelsBags(parcelsBagsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            {pageStatus.pageStatus == EPageStatus.OK ?
                <>
                    <div>
                        <Title title={"Parcel " + id} />
                    </div>

                    <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

                    <div className="float-end">
                        <EditRounded onClick={e => navigate("/parcels/edit/" + id)} className="hover-element" fontSize="large" />
                        <DeleteRounded onClick={(e) => deleteClicked(e.nativeEvent)} className="hover-element" fontSize="large" htmlColor="red" />
                    </div>

                    <table className="table">
                        <tbody>
                            <>
                                <tr className="form-control mb-2">
                                    <RowDisplay parcel={parcel} key={parcel.parcelNumber} />
                                </tr>
                            </>
                        </tbody>
                    </table>
                </>
                :
                <Loader {...pageStatus} />}
        </>
    )
}

export default ParcelDetails;