import { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { IShipment } from "../../domain/IShipment";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";

const RowDisplay = (props: { shipment: IShipment }) => {
    return (
        <td>
            <>
                <SubTitle2 subTitle={"Shipment Number"} />
                <RowItem item={props.shipment.shipmentNumber} />

                <SubTitle2 subTitle={"Finalized"} />
                <RowItem item={props.shipment.finalized} />
            </>
        </td>

    )
}

const RootIndex = () => {
    const [shipments, setShipments] = useState([] as IShipment[]);
    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const navigate = useNavigate();

    const loadData = async () => {
        let result = await BaseService.getAll<IShipment>('/shipments');
        if (result.ok && result.data) {
            setShipments(result.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: result.statusCode });
        }
        setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <Title title={"Shipments"} />
            <NavLink to={"/shipments/create"}>Create new shipment</NavLink>

            <table className="table">
                <tbody>
                    {shipments.map(shipment =>
                        <>
                            <tr className="form-control hover-element mb-3" onClick={e => navigate("/shipments/" + shipment.shipmentNumber)}>
                                <RowDisplay shipment={shipment} key={shipment.shipmentNumber} />
                            </tr>
                        </>
                    )}
                </tbody>
            </table>
            <Loader {...pageStatus} />
        </>
    )
}

export default RootIndex;