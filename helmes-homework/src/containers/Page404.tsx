const Page404 = () => {
    return (
        <div className="alert alert-danger">Page not found - 404!</div>
    );
}

export default Page404;