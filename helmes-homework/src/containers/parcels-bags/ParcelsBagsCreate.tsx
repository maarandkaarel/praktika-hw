import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import Alert, { EAlertClass } from "../../components/Alert";
import Loader from "../../components/Loader";
import Title from "../../components/Title";
import { IParcelsBagAdd } from "../../domain/IParcelsBag";
import { IShipment } from "../../domain/IShipment";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";

const ParcelsBagsCreate = () => {
    const { id } = useParams() as unknown as IRouteId;

    const [parcelsBag, setParcelsBag] = useState({} as IParcelsBagAdd);
    const [shipments, setShipments] = useState([] as IShipment[]);
    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessage] = useState('');
    const navigate = useNavigate();

    const createClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');

        if (parcelsBag.shipmentNumber === undefined && parcelsBag.shipmentNumber === null) {
            setAlertMessage('Got to choose shipment that the bag of parcels belongs to!');
        } else {
            let response = await BaseService.create<IParcelsBagAdd>('ParcelsBags', parcelsBag);
            if (response.ok){
                navigate('/shipments/' + parcelsBag.shipmentNumber)
            } else {
                setAlertMessage('ParcelsBag Creation unsuccessful')
            }
        }
    }

    const loadData = async () => {
        let shipmentsResponse = await BaseService.getAll<IShipment>('/Shipments');
        if (shipmentsResponse.ok && shipmentsResponse.data) {
            setShipments(shipmentsResponse.data);
            if (id !== undefined) {
                setParcelsBag({ ...parcelsBag, shipmentNumber: id });
            }
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: shipmentsResponse.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <Title title={"Create Parcels Bag"} />
            <form onSubmit={(e) => createClicked(e.nativeEvent)}>
                <div className="row">
                    <div className="col-md-6">
                        <section>
                            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />
                            <div className="form-group mb-2">
                                <label className="control-label" htmlFor="ParcelsBag_ShipmentNumber">ParcelsBags' Shipment Number</label>
                                <select className="form-control" id="ParcelsBag_ShipmentNumber" name="ParcelsBag.ShipmentNumber"
                                    onChange={ParcelsBag => setParcelsBag({ ...parcelsBag, shipmentNumber: ParcelsBag.target.value })}
                                    value={parcelsBag.shipmentNumber}>
                                    <option value="">--- Please select ---</option>
                                    {shipments.map(value => <option value={value.shipmentNumber} key={value.shipmentNumber}>{value.shipmentNumber}</option>)}
                                </select>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
            <div className="form-group mb-2 mt-2">
                <button onClick={(e) => { createClicked(e.nativeEvent) }} type="submit" className="btn btn-primary">Create</button>
            </div>
            <Loader {...pageStatus} />
        </>
    )
}

export default ParcelsBagsCreate;