import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { IParcel } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import EditRounded from '@material-ui/icons/EditRounded';
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import Alert, { EAlertClass } from "../../components/Alert";


const RowDisplay = (props: { parcelsBag: IParcelsBag, parcels: IParcel[] }) => {
    const navigate = useNavigate();
    return (
        <>
            <td>
                <SubTitle2 subTitle={"Parcels Bag Number"} />
                <RowItem item={props.parcelsBag.bagNumber} />

                <SubTitle2 subTitle={"Shipment Number"} />
                <RowItem item={props.parcelsBag.shipmentNumber} />
            </td>

            <SubTitle2 subTitle={"Parcels: " + props.parcels.filter(parcel => parcel.bagNumber == props.parcelsBag.bagNumber).length} />
            {props.parcels.map(parcel =>
                <>
                    <div className="form-control hover-element mb-2" onClick={e => navigate("/parcels/" + parcel.parcelNumber)}>
                        <SubTitle2 subTitle={"Parcel Number"} />
                        <RowItem item={parcel.parcelNumber} />

                        <SubTitle2 subTitle={"Recipients Name"} />
                        <RowItem item={parcel.recipientName} />

                        <SubTitle2 subTitle={"Destination Country"} />
                        <RowItem item={parcel.destinationCountry} />

                        <SubTitle2 subTitle={"Weight"} />
                        <RowItem item={parcel.weight} />

                        <SubTitle2 subTitle={"Price"} />
                        <RowItem item={parcel.price} />
                    </div>
                </>)}
        </>
    )
}

const ParcelsBagsDetails = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [parcelsBag, setParcelsBag] = useState({} as IParcelsBag);
    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const [parcels, setParcels] = useState([] as IParcel[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessge] = useState('');

    const deleteClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessge('');

        let response = await BaseService.delete('ParcelsBags/', id);
        if (response.ok) {
            navigate('/')
        } else {
            setAlertMessge("Parcels Bag Delete UNSUCCESSFUL!");
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    const loadData = async () => {
        let response = await BaseService.get<IParcelsBag>('ParcelsBags/', id);
        let parcelsBagsResponse = await BaseService.getAll<IParcelsBag>('/ParcelsBags')
        let parcelsResponse = await BaseService.getAll<IParcel>('/Parcels')
        if (response.ok && response.data && parcelsBagsResponse.ok && parcelsBagsResponse.data && parcelsResponse.ok && parcelsResponse.data) {
            setParcelsBag(response.data);
            setParcelsBags(parcelsBagsResponse.data);
            setParcels(parcelsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            {pageStatus.pageStatus == EPageStatus.OK ?
                <>
                    <div>
                        <Title title={"Parcels Bag " + id} />
                    </div>
                    <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

                    <div className="float-end">
                        <EditRounded onClick={e => navigate("/parcelsbags/edit/" + id)} className="hover-element" fontSize="large" />
                        <DeleteRounded onClick={(e) => deleteClicked(e.nativeEvent)} className="hover-element" fontSize="large" htmlColor="red" />
                    </div>

                    <table className="table">
                        <tbody>
                            <>

                                <tr className="form-control mb-2">
                                    <RowDisplay parcelsBag={parcelsBag} parcels={parcels.filter(parcel => parcel.bagNumber == parcelsBag.bagNumber)} key={parcelsBag.bagNumber} />
                                </tr>
                            </>
                        </tbody>
                    </table>
                </>
                :
                <Loader {...pageStatus} />}
        </>
    )
}

export default ParcelsBagsDetails;