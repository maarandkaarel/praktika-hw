import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { IParcel } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import Alert, { EAlertClass } from "../../components/Alert";

const RowDisplay = (props: { parcelsBag: IParcelsBag, parcels: IParcel[] }) => {
    const navigate = useNavigate();
    return (
        <>
            <td>
                <SubTitle2 subTitle={"Parcels Bag Number"} />
                <RowItem item={props.parcelsBag.bagNumber} />

                <SubTitle2 subTitle={"Shipment Number"} />
                <RowItem item={props.parcelsBag.shipmentNumber} />

                <SubTitle2 subTitle={"Count of Parcels"} />
                <RowItem item={props.parcels.length} />
           
                {/* <SubTitle2 subTitle={"Parcels: "} /> */}
            </td>

            {/* {props.parcels.map(parcel =>
                <>
                    <td className="form-control mb-2" onClick={e => navigate("/parcels/" + parcel.parcelNumber)}>
                        <SubTitle2 subTitle={"Parcel Number"} />
                        <RowItem item={parcel.parcelNumber} />

                        <SubTitle2 subTitle={"Parcels Bag Number"} />
                        <RowItem item={parcel.bagNumber} />

                        <SubTitle2 subTitle={"Destination Country"} />
                        <RowItem item={parcel.destinationCountry} />

                        <SubTitle2 subTitle={"Recipients Name"} />
                        <RowItem item={parcel.recipientName} />

                        <SubTitle2 subTitle={"Weight"} />
                        <RowItem item={parcel.weight} />

                        <SubTitle2 subTitle={"Price"} />
                        <RowItem item={parcel.price} />
                    </td>
                </>)} */}
        </>
    )
}


const ParcelsBagsIndex = () => {

    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const [parcels, setParcels] = useState([] as IParcel[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessge] = useState('');

    const loadData = async () => {
        let response = await BaseService.getAll<IParcelsBag>('/ParcelsBags')
        let parcelsResponse = await BaseService.getAll<IParcel>('/Parcels')
        if (response.ok && response.data && parcelsResponse.ok && parcelsResponse.data) {
            setParcelsBags(response.data);
            setParcels(parcelsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <div>
                <Title title={"Parcels Bags"} />
                <Link to={"/parcelsBags/create"}>Add new Parcels Bag</Link>
            </div>

            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

            <table className="table">
                <tbody>
                    <>
                        {parcelsBags.map(parcelsBag =>
                            <>
                                <tr className="form-control hover-element mb-3" onClick={e => navigate("/parcelsBags/" + parcelsBag.bagNumber)}>
                                    <RowDisplay parcelsBag={parcelsBag} parcels={parcels.filter(parcel => parcel.bagNumber == parcelsBag.bagNumber)} key={parcelsBag.bagNumber} />
                                </tr>
                            </>
                        )}
                    </>
                </tbody>
            </table>
            <Loader {...pageStatus} />
        </>
    )
}

export default ParcelsBagsIndex;