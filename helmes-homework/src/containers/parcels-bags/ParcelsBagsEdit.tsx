import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle from "../../components/SubTitle";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { IParcel } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { IShipment } from "../../domain/IShipment";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import Alert, { EAlertClass } from "../../components/Alert";

const ParcelRowDisplay = (props: { parcel: IParcel }) => {
    return (
        <>
            <SubTitle2 subTitle={"Parcel Number"} />
            <RowItem item={props.parcel.parcelNumber} />
            <SubTitle2 subTitle={"Recipients Name"} />
            <RowItem item={props.parcel.recipientName} />
            <SubTitle2 subTitle={"Destination Country"} />
            <RowItem item={props.parcel.destinationCountry} />
            <SubTitle2 subTitle={"Weight"} />
            <RowItem item={props.parcel.weight} />
            <SubTitle2 subTitle={"Price"} />
            <RowItem item={props.parcel.price} />
            <SubTitle2 subTitle={"Parcels Bag Number"} />
            <RowItem item={props.parcel.bagNumber} />
        </>

    )
}

const ParcelsBagsEdit = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [shipments, setShipments] = useState([] as IShipment[]);
    const [parcelsBag, setParcelsBag] = useState({} as IParcelsBag);
    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const [parcels, setParcels] = useState([] as IParcel[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const deleteClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        setSuccessMessage('');
        let response = await BaseService.delete('ParcelsBags/', id);
        if (response.ok) {
            navigate('/shipments/' + parcelsBag.shipmentNumber)
        } else {
            setAlertMessage("Parcels Bag Delete UNSUCCESSFUL!");
        }
    }

    const editClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        setSuccessMessage('');
        if (parcelsBag.shipmentNumber === undefined || parcelsBag.shipmentNumber === '' || parcelsBag.bagNumber === undefined || parcelsBag.bagNumber === '') {
            setAlertMessage("Empty fields!");
        } else {
            let response = await BaseService.edit<IParcelsBag>('ParcelsBags/', parcelsBag.bagNumber, parcelsBag);
            if (response.ok) {
                navigate('/shipments/' + parcelsBag.shipmentNumber)
            } else {
                setAlertMessage('Parcels bag edit UNSUCCESSFUL!');
            }
        }
    }

    const loadData = async () => {
        let response = await BaseService.get<IParcelsBag>('ParcelsBags/', id);
        let shipmentsResponse = await BaseService.getAll<IShipment>('/Shipments')
        let parcelsBagsResponse = await BaseService.getAll<IParcelsBag>('/ParcelsBags')
        let parcelsResponse = await BaseService.getAll<IParcel>('/Parcels')
        if (response.ok && response.data && shipmentsResponse.ok && shipmentsResponse.data &&
            parcelsBagsResponse.ok && parcelsBagsResponse.data && parcelsResponse.ok && parcelsResponse.data) {
            setParcelsBag(response.data);
            setShipments(shipmentsResponse.data);
            setParcelsBags(parcelsBagsResponse.data);
            setParcels(parcelsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            {pageStatus.pageStatus == EPageStatus.OK ?
                <>
                    <div>
                        <Title title={"Edit Parcels Bag " + parcelsBag.bagNumber} />
                    </div>

                    <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />
                    <Alert show={successMessage !== ''} message={successMessage} alertClass={EAlertClass.Success} />

                    <div className="float-end">
                        <DeleteRounded onClick={(e) => deleteClicked(e.nativeEvent)} className="hover-element" fontSize="large" htmlColor="red" />
                    </div>

                    <table className="table">
                        <tbody>
                            <>
                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="ParcelsBagNumber">Bag Number</label>
                                    <input className="form-control" disabled={true} type="text" id="ParcelsBagNumber" maxLength={8} name="ParcelsBagNumber"
                                        value={parcelsBag.bagNumber} onChange={bagNr => setParcelsBag({ ...parcelsBag, bagNumber: bagNr.target.value })} />
                                </tr>

                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="ParcelsBag_ShipmentNumber">Shipment</label>
                                    <select className="form-control" id="ParcelsBag_ShipmentNumber" name="ParcelsBag.ShipmentNumber"
                                        onChange={shipment => setParcelsBag({ ...parcelsBag, shipmentNumber: shipment.target.value, shipment: null })}
                                        value={parcelsBag.shipmentNumber}>
                                        <option value="">--- Please select ---</option>
                                        {shipments.map(value => <option value={value.shipmentNumber} key={value.shipmentNumber}>{value.shipmentNumber}</option>)}
                                    </select>
                                </tr>

                                <tr className="form-group mb-2 mt-2">
                                    <td>
                                        <button onClick={(e) => editClicked(e.nativeEvent)} type="submit" className="btn btn-primary">Edit</button>
                                    </td>
                                </tr>

                                <tr className="form-control">
                                    <SubTitle subTitle={"Parcels"} />
                                    <Link to={"/parcels/create/" + id}>Add new Parcel</Link>
                                    {parcels.filter(parcel => parcel.bagNumber == parcelsBag.bagNumber).map(parcel =>
                                        <>
                                            <td className="form-control hover-element mb-2" onClick={e => navigate("/parcels/edit/" + parcel.parcelNumber)}>
                                                <ParcelRowDisplay parcel={parcel} key={parcelsBag.bagNumber} />
                                            </td>
                                        </>
                                    )}
                                </tr>
                            </>
                        </tbody>
                    </table>
                </>
                :
                <Loader {...pageStatus} />}
        </>
    )
}

export default ParcelsBagsEdit;