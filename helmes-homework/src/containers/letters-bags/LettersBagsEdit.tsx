import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import Title from "../../components/Title";
import { ILettersBag } from "../../domain/ILettersBag";
import { IShipment } from "../../domain/IShipment";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import Alert, { EAlertClass } from "../../components/Alert";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { transpile } from "typescript";

const LettersBagsEdit = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [shipments, setShipments] = useState([] as IShipment[]);
    const [lettersBag, setLettersBag] = useState({} as ILettersBag);
    const [lettersBags, setLettersBags] = useState([] as ILettersBag[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const deleteClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        setSuccessMessage('');
        let response = await BaseService.delete('LettersBags/', id);
        if (response.ok) {
            navigate('/shipments/' + lettersBag.shipmentNumber)
        } else {
            setAlertMessage("Letters Bag Delete UNSUCCESSFUL!");
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    const editClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        setSuccessMessage('');
        if (lettersBag.bagNumber === undefined || lettersBag.bagNumber === '' || lettersBag.shipmentNumber === undefined || lettersBag.shipmentNumber === '' ||
            lettersBag.countOfLetters === undefined || lettersBag.price === undefined || lettersBag.weight === undefined) {
            setAlertMessage("Empty fields!");
        } else if (lettersBag.countOfLetters == 0) {
            setAlertMessage('Letters Bag has to contain atleast 1 letter');
        } else {
            let response = await BaseService.edit<ILettersBag>('lettersBags/', id, lettersBag);
            if (response.ok) {
                setSuccessMessage('Letters bag edit SUCCESSFUL!');
                navigate('/shipments/' + lettersBag.shipmentNumber)
            } else {
                setAlertMessage('Letters bag edit UNSUCCESSFUL!');
            }
        }
    }

    const addCount = (e: Event) => {
        e.preventDefault();
        if (lettersBag.countOfLetters !== undefined) {
            setLettersBag({ ...lettersBag, countOfLetters: lettersBag.countOfLetters += 1 })
        } else {
            setLettersBag({ ...lettersBag, countOfLetters: 1 })
        }
    }

    const subCount = (e: Event) => {
        e.preventDefault();
        if (lettersBag.countOfLetters !== undefined && lettersBag.countOfLetters > 0) {
            setLettersBag({ ...lettersBag, countOfLetters: lettersBag.countOfLetters -= 1 })
        }
    }

    function toFixedTrunc(x: number, n: number) {
        const v = (typeof x === 'string' ? x : x.toString()).split('.');
        if (n <= 0) {
            return parseFloat(v[0]);
        }
        let f = v[1] || '';
        if (f.length > n) {
            return parseFloat(`${v[0]}.${f.substr(0, n)}`);
        }
        while (f.length < n) f += '0';

        return parseFloat(`${v[0]}.${f}`);
    }

    const loadData = async () => {
        let response = await BaseService.get<ILettersBag>('LettersBags/', id);
        let shipmentsResponse = await BaseService.getAll<IShipment>('/Shipments')
        let lettersBagsResponse = await BaseService.getAll<ILettersBag>('/LettersBags')
        if (response.ok && response.data && shipmentsResponse.ok && shipmentsResponse.data &&
            lettersBagsResponse.ok && lettersBagsResponse.data) {
            setLettersBag(response.data);
            setShipments(shipmentsResponse.data);
            setLettersBags(lettersBagsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            {pageStatus.pageStatus == EPageStatus.OK ?
                <>
                    <div>
                        <Title title={"Edit Letters Bag " + lettersBag.bagNumber} />
                    </div>

                    <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />
                    <Alert show={successMessage !== ''} message={successMessage} alertClass={EAlertClass.Success} />

                    <div className="float-end">
                        <DeleteRounded onClick={(e) => deleteClicked(e.nativeEvent)} className="hover-element" fontSize="large" htmlColor="red" />
                    </div>

                    <table className="table">
                        <tbody>
                            <>
                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="LettersBagNumber">Bag Number</label>
                                    <input className="form-control" disabled={true} type="text" id="LettersBagNumber" maxLength={8} name="LettersBagNumber"
                                        value={lettersBag.bagNumber} onChange={bagNr => setLettersBag({ ...lettersBag, bagNumber: bagNr.target.value })} />
                                </tr>

                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="LettersBag_ShipmentNumber">Shipment</label>
                                    <select className="form-control" id="LettersBag_ShipmentNumber" name="LettersBag.ShipmentNumber"
                                        value={lettersBag.shipmentNumber}
                                        onChange={shipment => setLettersBag({ ...lettersBag, shipmentNumber: shipment.target.value, shipment: null })}>
                                        <option value="">--- Please select ---</option>
                                        {shipments.map(value => <option value={value.shipmentNumber} key={value.shipmentNumber}>{value.shipmentNumber}</option>)}
                                    </select>
                                </tr>

                                <label htmlFor="LettersBag_Count">Count of Letters</label>
                                <InputGroup>
                                    <Form.Control
                                        placeholder="Count of letters in the bag"
                                        value={lettersBag.countOfLetters}
                                    />
                                    <Button variant="outline-secondary" onClick={e => addCount(e.nativeEvent)}>+</Button>
                                    <Button variant="outline-secondary" onClick={e => subCount(e.nativeEvent)}>-</Button>
                                </InputGroup>

                                <tr className="form-group mb-2">
                                    <label htmlFor="LettersBag_Weight">Weight</label>
                                    <input value={lettersBag.weight}
                                        onChange={e => setLettersBag({ ...lettersBag, weight: toFixedTrunc(e.target.valueAsNumber, 3) })}
                                        className="form-control" type="number" id="LettersBag_Weight" name="Input.Weight" placeholder="Weight of the letters bag"
                                        min="0" step="0.001"/>
                                </tr>

                                <tr className="form-group mb-2">
                                    <label htmlFor="LettersBag_Price">Price €</label>
                                    <input value={lettersBag.price}
                                        onChange={e => setLettersBag({ ...lettersBag, price: toFixedTrunc(e.target.valueAsNumber, 2) })}
                                        className="form-control" type="number" id="LettersBag_Price" name="Input.Price" placeholder="Price of the bag"
                                        min="0" step="0.01"/>
                                </tr>

                                <tr className="form-group mb-2 mt-2">
                                    <td>
                                        <button onClick={(e) => editClicked(e.nativeEvent)} type="submit" className="btn btn-primary">Edit</button>
                                    </td>
                                </tr>
                            </>
                        </tbody>
                    </table>
                </>
                :
                <Loader {...pageStatus} />}
        </>
    )
}

export default LettersBagsEdit;