import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { ILettersBag } from "../../domain/ILettersBag";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import Alert, { EAlertClass } from "../../components/Alert";

const RowDisplay = (props: { lettersBag: ILettersBag }) => {
    return (
        <>
            <td>
                <SubTitle2 subTitle={"Letters Bag Number"} />
                <RowItem item={props.lettersBag.bagNumber} />

                <SubTitle2 subTitle={"Shipment Number"} />
                <RowItem item={props.lettersBag.shipmentNumber} />

                <SubTitle2 subTitle={"Count of Letters"} />
                <RowItem item={props.lettersBag.countOfLetters} />

                <SubTitle2 subTitle={"Weight"} />
                <RowItem item={props.lettersBag.weight} />

                <SubTitle2 subTitle={"Price"} />
                <RowItem item={props.lettersBag.price} />
            </td>
        </>
    )
}


const LettersBagsIndex = () => {

    const [lettersBags, setLettersBags] = useState([] as ILettersBag[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessge] = useState('');

    const loadData = async () => {
        let response = await BaseService.getAll<ILettersBag>('/LettersBags');
        if (response.ok && response.data) {
            setLettersBags(response.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <div>
                <Title title={"Letters Bags"} />
                <Link to={"/lettersBags/create"}>Add new Letters Bag</Link>
            </div>

            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

            <table className="table">
                <tbody>
                    <>
                        {lettersBags.map(lettersBag =>
                            <>
                                <tr className="form-control hover-element mb-3" onClick={e => navigate("/lettersBags/" + lettersBag.bagNumber)}>
                                    <RowDisplay lettersBag={lettersBag} key={lettersBag.bagNumber} />
                                </tr>
                            </>
                        )}
                    </>
                </tbody>
            </table>
            <Loader {...pageStatus} />
        </>
    )
}

export default LettersBagsIndex;