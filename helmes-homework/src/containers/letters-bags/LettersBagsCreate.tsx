import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import Alert, { EAlertClass } from "../../components/Alert";
import Loader from "../../components/Loader";
import Title from "../../components/Title";
import { ILettersBagAdd } from "../../domain/ILettersBag";
import { IShipment } from "../../domain/IShipment";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

const LettersBagsCreate = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [lettersBag, setLettersBag] = useState({} as ILettersBagAdd);
    const [shipments, setShipments] = useState([] as IShipment[]);
    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessage] = useState('');
    const navigate = useNavigate();

    const createClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');

        if (lettersBag.shipmentNumber === undefined || lettersBag.shipmentNumber === '' || lettersBag.countOfLetters === undefined ||
            lettersBag.price === undefined || lettersBag.weight === undefined) {
            setAlertMessage('Empty input fields!');
        } else if (lettersBag.countOfLetters == 0) {
            setAlertMessage('Letters Bag has to contain atleast 1 letter');
        } else {
            let response = await BaseService.create<ILettersBagAdd>('LettersBags', lettersBag);
            if (response.ok){
                navigate('/shipments/' + lettersBag.shipmentNumber)
            } else {
                setAlertMessage('LettersBag Creation unsuccessful')
            }
        }
    }

    const addCount = (e: Event) => {
        e.preventDefault();
        if (lettersBag.countOfLetters !== undefined) {
            setLettersBag({ ...lettersBag, countOfLetters: lettersBag.countOfLetters += 1 })
        } else {
            setLettersBag({ ...lettersBag, countOfLetters: 1 })
        }
    }

    const subCount = (e: Event) => {
        e.preventDefault();
        if (lettersBag.countOfLetters !== undefined && lettersBag.countOfLetters > 0) {
            setLettersBag({ ...lettersBag, countOfLetters: lettersBag.countOfLetters -= 1 })
        }
    }

    
    function toFixedTrunc(x: number, n: number) {
        const v = (typeof x === 'string' ? x : x.toString()).split('.');
        if (n <= 0) {
            return parseFloat(v[0]);
        }
        let f = v[1] || '';
        if (f.length > n) {
            return parseFloat(`${v[0]}.${f.substr(0, n)}`);
        }
        while (f.length < n) f += '0';

        return parseFloat(`${v[0]}.${f}`);
    }


    const loadData = async () => {
        let shipmentsResponse = await BaseService.getAll<IShipment>('Shipments');
        if (shipmentsResponse.ok && shipmentsResponse.data) {
            setShipments(shipmentsResponse.data);
            if (id !== undefined) {
                setLettersBag({ ...lettersBag, shipmentNumber: id });
            }
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: shipmentsResponse.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            <Title title={"Create Letters Bag"} />
            <form onSubmit={(e) => createClicked(e.nativeEvent)}>
                <div className="row">
                    <div className="col-md-6">
                        <section>

                            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

                            <div className="form-group mb-2">
                                <label className="control-label" htmlFor="LettersBag_ShipmentNumber">LettersBags' Shipment Number</label>
                                <select className="form-control" id="LettersBag_ShipmentNumber" name="LettersBag.ShipmentNumber"
                                    onChange={shipment => setLettersBag({ ...lettersBag, shipmentNumber: shipment.target.value })}
                                    value={lettersBag.shipmentNumber}>
                                    <option value="">--- Please select ---</option>
                                    {shipments.map(value => <option value={value.shipmentNumber} key={value.shipmentNumber}>{value.shipmentNumber}</option>)}
                                </select>
                            </div>

                            <label htmlFor="LettersBag_Count">Count of Letters</label>
                            <InputGroup>
                                <Form.Control
                                    placeholder="Count of letters in the bag"
                                    value={lettersBag.countOfLetters}
                                />
                                <Button variant="outline-secondary" onClick={e => addCount(e.nativeEvent)}>+</Button>
                                <Button variant="outline-secondary" onClick={e => subCount(e.nativeEvent)}>-</Button>
                            </InputGroup>

                            <div className="form-group mb-2">
                                <label htmlFor="LettersBag_Weight">Weight</label>
                                <input value={lettersBag.weight}
                                    onChange={e => setLettersBag({ ...lettersBag, weight: toFixedTrunc(e.target.valueAsNumber, 3) })}
                                    className="form-control" type="number" id="LettersBag_Weight" name="Input.Weight" placeholder="Weight of the letters bag" 
                                    min="0" step="0.001"/>
                            </div>

                            <div className="form-group mb-2">
                                <label htmlFor="LettersBag_Price">Price €</label>
                                <input value={lettersBag.price}
                                    onChange={e => setLettersBag({ ...lettersBag, price: toFixedTrunc(e.target.valueAsNumber, 2) })}
                                    className="form-control" type="number" id="LettersBag_Price" name="Input.Price" placeholder="Price of the letters"
                                    min="0" step="0.01"
                                />
                            </div>
                        </section>
                    </div>
                </div>
            </form>
            <div className="form-group mb-2 mt-2">
                <button onClick={(e) => { createClicked(e.nativeEvent) }} type="submit" className="btn btn-primary">Create</button>
            </div>
            <Loader {...pageStatus} />
        </>
    )
}

export default LettersBagsCreate;