import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { ILettersBag } from "../../domain/ILettersBag";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import EditRounded from '@material-ui/icons/EditRounded';
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import Alert, { EAlertClass } from "../../components/Alert";


const RowDisplay = (props: { lettersBag: ILettersBag }) => {
    return (
        <>
            <td>
                <SubTitle2 subTitle={"Letters Bag Number"} />
                <RowItem item={props.lettersBag.bagNumber} />

                <SubTitle2 subTitle={"Shipment Number"} />
                <RowItem item={props.lettersBag.shipmentNumber} />

                <SubTitle2 subTitle={"Count of Letters"} />
                <RowItem item={props.lettersBag.countOfLetters} />

                <SubTitle2 subTitle={"Weight"} />
                <RowItem item={props.lettersBag.weight} />

                <SubTitle2 subTitle={"Price"} />
                <RowItem item={props.lettersBag.price} />
            </td>
        </>
    )
}


const LettersBagsDetails = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [lettersBag, setLettersBag] = useState({} as ILettersBag);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessge] = useState('');

    const deleteClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessge('');

        let response = await BaseService.delete('LettersBags/', id);
        if (response.ok) {
            navigate('/shipments/' + lettersBag.shipmentNumber)
        } else {
            setAlertMessge("Letters Bag Delete UNSUCCESSFUL!");
        }
    }

    const loadData = async () => {
        let response = await BaseService.get<ILettersBag>('LettersBags/', id);
        if (response.ok && response.data) {
            setLettersBag(response.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            {pageStatus.pageStatus == EPageStatus.OK ?
                <>
                    <div>
                        <Title title={"Letters Bag " + id} />
                    </div>

                    <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />
                    
                    <div className="float-end">
                        <EditRounded onClick={e => navigate("/lettersBags/edit/" + id)} className="hover-element" fontSize="large" />
                        <DeleteRounded onClick={(e) => deleteClicked(e.nativeEvent)} className="hover-element" fontSize="large" htmlColor="red" />
                    </div>

                    <table className="table">
                        <tbody>
                            <>
                                <tr className="form-control mb-2">
                                    <RowDisplay lettersBag={lettersBag} key={lettersBag.bagNumber} />
                                </tr>
                            </>
                        </tbody>
                    </table>
                </>
                :
                <Loader {...pageStatus} />}
        </>
    )
}

export default LettersBagsDetails;