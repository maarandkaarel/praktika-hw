import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle from "../../components/SubTitle";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { ILettersBag } from "../../domain/ILettersBag";
import { IParcel } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { IShipment } from "../../domain/IShipment";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import EditRounded from '@material-ui/icons/EditRounded';
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import Alert, { EAlertClass } from "../../components/Alert";
import { EAirPort } from "../../types/EAirport";

const RowDisplay = (props: { shipment: IShipment }) => {
    return (
        <td>
            <SubTitle subTitle={"Shipment Number"} />
            <RowItem item={props.shipment.shipmentNumber} />

            <SubTitle subTitle={"Finalized"} />
            <RowItem item={props.shipment.finalized} />

            <SubTitle subTitle={"Flight Number"} />
            <RowItem item={props.shipment.flightNumber} />

            <SubTitle subTitle={"Flight Date"} />
            <RowItem item={props.shipment.flightDate.toLocaleString()} />

            <SubTitle subTitle={"Airport"} />
            <RowItem item={Object.values(EAirPort)[props.shipment.airport]} />
        </td>
    )
}

const LettersBagRowDisplay = (props: { lettersBag: ILettersBag }) => {
    return (
        <>
            <SubTitle2 subTitle={"Bag Number"} />
            <RowItem item={props.lettersBag.bagNumber} />

            <SubTitle2 subTitle={"Count of Letters"} />
            <RowItem item={props.lettersBag.countOfLetters} />

            <SubTitle2 subTitle={"Weight"} />
            <RowItem item={props.lettersBag.weight} />

            <SubTitle2 subTitle={"Price"} />
            <RowItem item={props.lettersBag.price} />

            <SubTitle2 subTitle={"Shipment Number"} />
            <RowItem item={props.lettersBag.shipmentNumber} />
        </>
    )
}

const ParcelsBagRowDisplay = (props: { parcelsBag: IParcelsBag, parcels: IParcel[] }) => {

    let totalPrice = 0;
    props.parcels.forEach(parcel => {
        totalPrice += parcel.price;
    });

    return (
        <>
            <SubTitle2 subTitle={"Parcels Bag Number"} />
            <RowItem item={props.parcelsBag.bagNumber} />

            <SubTitle2 subTitle={"Shipment Number"} />
            <RowItem item={props.parcelsBag.shipmentNumber} />

            <SubTitle2 subTitle={"Count of Parcels"} />
            <RowItem item={props.parcels.length} />

            <SubTitle2 subTitle={"Total Price"}/>
            <RowItem item={totalPrice}/>
        </>
    )
}


const ShipmentDetails = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [shipment, setShipment] = useState({} as IShipment);
    const [lettersBags, setLettersBags] = useState([] as ILettersBag[]);
    const [parcelsBag, setParcelsBag] = useState({} as IParcelsBag);
    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const [parcels, setParcels] = useState([] as IParcel[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessge] = useState('');

    const deleteClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessge('');

        let response = await BaseService.delete('Shipments/', id);
        if (response.ok) {
            navigate('/')
        } else {
            setAlertMessge("Shipment Delete UNSUCCESSFUL!");
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    const loadData = async () => {
        let response = await BaseService.get<IShipment>('Shipments/', id);
        let lettersBagsResponse = await BaseService.getAll<ILettersBag>('/LettersBags')
        let parcelsBagsResponse = await BaseService.getAll<IParcelsBag>('/ParcelsBags')
        let parcelsResponse = await BaseService.getAll<IParcel>('/Parcels')
        if (response.ok && response.data && lettersBagsResponse.ok && lettersBagsResponse.data &&
            parcelsBagsResponse.ok && parcelsBagsResponse.data && parcelsResponse.ok && parcelsResponse.data) {
            setShipment(response.data);
            setParcelsBags(parcelsBagsResponse.data);
            setLettersBags(lettersBagsResponse.data);
            setParcels(parcelsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <>
            {pageStatus.pageStatus == EPageStatus.OK ?
                <>
                    <div>
                        <Title title={"Shipment " + id} />
                    </div>
                    <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

                    <div className="float-end">
                        <EditRounded onClick={e => navigate("/shipments/edit/" + id)} className="hover-element" fontSize="large" />
                        <DeleteRounded onClick={(e) => deleteClicked(e.nativeEvent)} className="hover-element" fontSize="large" htmlColor="red" />
                    </div>

                    <table className="table">
                        <tbody>
                            <>
                                <tr className="form-control mb-3">
                                    <RowDisplay shipment={shipment} key={shipment.shipmentNumber} />
                                </tr>
                                <tr className="form-control mb-3">
                                    <td>
                                        <SubTitle subTitle={"Letters Bags: " + lettersBags.filter(lettersBag => lettersBag.shipmentNumber == shipment.shipmentNumber).length} />
                                    </td>
                                    {lettersBags.filter(lettersBag => lettersBag.shipmentNumber == shipment.shipmentNumber).map(lettersBag =>
                                        <>
                                            <td className="form-control hover-element mb-2" onClick={e => navigate("/lettersBags/" + lettersBag.bagNumber)}>
                                                <LettersBagRowDisplay lettersBag={lettersBag} key={lettersBag.bagNumber} />
                                            </td>
                                        </>
                                    )}
                                </tr>
                                <tr className="form-control mb-3">
                                    <td>
                                        <SubTitle subTitle={"Parcels Bags: " + parcelsBags.filter(parcelsBag => parcelsBag.shipmentNumber == shipment.shipmentNumber).length} />
                                    </td>
                                    {parcelsBags.filter(parcelsBag => parcelsBag.shipmentNumber == shipment.shipmentNumber).map(parcelsBag =>
                                        <>
                                            <td className="form-control hover-element mb-2" onClick={e => navigate("/parcelsBags/" + parcelsBag.bagNumber)}>
                                                <ParcelsBagRowDisplay parcelsBag={parcelsBag}
                                                    parcels={parcels.filter(parcel => parcel.bagNumber == parcelsBag.bagNumber)} key={parcelsBag.bagNumber} />
                                            </td>
                                        </>
                                    )}
                                </tr>
                            </>
                        </tbody>
                    </table>
                </>
                :
                <Loader {...pageStatus} />}
        </>
    )
}

export default ShipmentDetails;