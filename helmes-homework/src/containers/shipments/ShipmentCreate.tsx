import React, { useState } from "react";
import { EAlertClass } from "../../components/Alert";
import { useNavigate } from "react-router-dom";
import Title from "../../components/Title";
import { IShipmentAdd } from "../../domain/IShipment";
import { BaseService } from "../../services/base-service";
import { EPageStatus } from "../../types/EPageStatus";
import Alert from "../../components/Alert";
import { EAirPort } from "../../types/EAirport";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';


const ShipmentCreate = () => {
    const [shipment, setShipment] = useState({} as IShipmentAdd);
    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessage] = useState('');
    const navigate = useNavigate();

    const createClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        
        if (shipment.airport === undefined || shipment.flightDate === undefined) {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: 666 });
            setAlertMessage('Empty fields!');
        }
        if (shipment.flightNumber !== undefined && !shipment.flightNumber.match("[a-zA-Z]{2}[0-9]{4}") && shipment.flightNumber.length !== 6) {
            setAlertMessage('Flight Number does not match the rules LLNNNN!')
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: 666 });
        }

        if (shipment.airport !== undefined && shipment.flightNumber !== undefined && shipment.flightNumber.match("[a-zA-Z]{2}[0-9]{4}") 
        && shipment.flightDate !== undefined) {
            let response = await BaseService.create<IShipmentAdd>("Shipments", shipment);
            if (response.ok){
                navigate('/')
            } else {
                setAlertMessage('Shipments Creation UNSUCCESSFUL')
            }
        }
    }

    // finalized should be disabled/not existent on creation and only on edit/details
    function handleFinalized() {
        if (shipment.flightDate !== null && shipment.flightDate > new Date()) {
            setShipment({ ...shipment, finalized: !shipment.finalized })
        }
    }

    function handleAirport(e: React.ChangeEvent<HTMLSelectElement>) {
        if (e.target.value === 'TLL') {
            setShipment({ ...shipment, airport: 0 })
        }
        if (e.target.value === 'RIX') {
            setShipment({ ...shipment, airport: 1 })
        }
        if (e.target.value === 'HEL') {
            setShipment({ ...shipment, airport: 2 })
        }
    }

    return (
        <>
            <Title title={"Create Shipment"} />
            <form onSubmit={(e) => createClicked(e.nativeEvent)}>
                <div className="row">
                    <div className="col-md-6">
                        <section>
                            <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />

                            <div className="form-group mb-2">
                                <label className="control-label" htmlFor="Shipment_Airport">Airport</label>
                                <select className="form-control" id="Shipment_Airport" name="Shipment.Airport"
                                    onChange={e => handleAirport(e)}>
                                    <option value={""}>--- Please select ---</option>
                                    {Object.keys(EAirPort).filter(airport => isNaN(Number(airport))).map(value => <option value={value} key={value}>{value}</option>)}
                                </select>
                            </div>

                            <div className="form-group mb-2">
                                <label htmlFor="Shipment_FlightNumber">Flight Number</label>
                                <input value={shipment.flightNumber}
                                    onChange={e => setShipment({ ...shipment, flightNumber: e.target.value })}
                                    className="form-control" type="string" id="Shipment_FlightNumber" name="Input.FlightNumber" placeholder="FlightNumber LLNNNN" 
                                    maxLength={8}/>
                            </div>

                            <div className="form-group mb-2">
                                <label className="control-label" htmlFor="Shipment_FlightDate">Flight Date </label>
                                <DatePicker
                                    selected={shipment.flightDate}
                                    onChange={e => setShipment({ ...shipment, flightDate: new Date(e!.toString()) })}
                                    showTimeSelect
                                    timeIntervals={1}
                                    dateFormat="dd/MM/yyyy HH:mm"
                                    timeFormat="HH:mm"
                                    className="form-control"
                                    placeholderText="Select flight date and time"
                                />
                            </div>
                            
                            {/* <div>
                                <label htmlFor="Input_Finalized">Finalized</label>
                                <input type="checkbox" checked={shipment.finalized}
                                    disabled={true}
                                    onChange={(e) => handleFinalized()}
                                    className="form-check-input" id="Input_Finalized" name="Input.Finalized" />
                            </div> */}
                        </section>
                    </div>
                </div>
            </form>
            <div className="form-group mt-2 mb-2">
                <button onClick={(e) => { createClicked(e.nativeEvent) }} type="submit" className="btn btn-primary">Create</button>
            </div>
        </>
    )
}

export default ShipmentCreate;