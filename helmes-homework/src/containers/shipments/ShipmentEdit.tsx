import { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import RowItem from "../../components/RowItem";
import SubTitle from "../../components/SubTitle";
import SubTitle2 from "../../components/SubTitle2";
import Title from "../../components/Title";
import { ILettersBag } from "../../domain/ILettersBag";
import { IParcel } from "../../domain/IParcel";
import { IParcelsBag } from "../../domain/IParcelsBag";
import { IShipment } from "../../domain/IShipment";
import { BaseService } from "../../services/base-service";
import { EAirPort } from "../../types/EAirport";
import { EPageStatus } from "../../types/EPageStatus";
import { IRouteId } from "../../types/IRouteId";
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import Alert, { EAlertClass } from "../../components/Alert";
import DatePicker from 'react-datepicker';

const LettersBagRowDisplay = (props: { lettersBag: ILettersBag }) => {
    return (
        <>
            <SubTitle2 subTitle={"Bag Number"} />
            <RowItem item={props.lettersBag.bagNumber} />

            <SubTitle2 subTitle={"Count of Letters"} />
            <RowItem item={props.lettersBag.countOfLetters} />

            <SubTitle2 subTitle={"Weight"} />
            <RowItem item={props.lettersBag.weight} />

            <SubTitle2 subTitle={"Price"} />
            <RowItem item={props.lettersBag.price} />

            <SubTitle2 subTitle={"Shipment Number"} />
            <RowItem item={props.lettersBag.shipmentNumber} />
        </>
    )
}

const ParcelsBagRowDisplay = (props: { parcelsBag: IParcelsBag, parcels: IParcel[] }) => {

    let totalPrice = 0;
    props.parcels.forEach(parcel => {
        totalPrice += parcel.price;
    });

    return (
        <>
            <SubTitle2 subTitle={"Parcels Bag Number"} />
            <RowItem item={props.parcelsBag.bagNumber} />

            <SubTitle2 subTitle={"Shipment Number"} />
            <RowItem item={props.parcelsBag.shipmentNumber} />

            <SubTitle2 subTitle={"Count of Parcels"} />
            <RowItem item={props.parcels.length} />

            <SubTitle2 subTitle={"Total Price"} />
            <RowItem item={totalPrice} />
        </>
    )
}


const ShipmentEdit = () => {

    const { id } = useParams() as unknown as IRouteId;

    const [shipment, setShipment] = useState({} as IShipment);
    const [lettersBags, setLettersBags] = useState([] as ILettersBag[]);
    const [parcelsBag, setParcelsBag] = useState({} as IParcelsBag);
    const [parcelsBags, setParcelsBags] = useState([] as IParcelsBag[]);
    const [parcels, setParcels] = useState([] as IParcel[]);
    const navigate = useNavigate();

    const [pageStatus, setPageStatus] = useState({ pageStatus: EPageStatus.Loading, statusCode: -1 });
    const [alertMessage, setAlertMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const deleteClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        setSuccessMessage('');
        let response = await BaseService.delete('Shipments/', id);
        if (response.ok) {
            navigate('/')
        } else {
            setAlertMessage("Shipment Delete UNSUCCESSFUL!");
        }
    }

    const editClicked = async (e: Event) => {
        e.preventDefault();
        setAlertMessage('');
        setSuccessMessage('');

        console.log(shipment);

        if (shipment.shipmentNumber === undefined || shipment.shipmentNumber === '' || shipment.airport === undefined || shipment.airport === null || shipment.flightDate === undefined ||
            shipment.flightNumber === undefined || shipment.flightNumber === '' || shipment.finalized === undefined) {
            setAlertMessage("Empty fields!");
        } else {
            let response = await BaseService.edit<IShipment>('shipments/', id, shipment);
            if (response.ok) {
                navigate('/')
            } else {
                setAlertMessage('Shipment edit UNSUCCESSFUL!');
            }
        }
    }

    const loadData = async () => {
        let response = await BaseService.get<IShipment>('Shipments/', id);
        let lettersBagsResponse = await BaseService.getAll<ILettersBag>('/LettersBags')
        let parcelsBagsResponse = await BaseService.getAll<IParcelsBag>('/ParcelsBags')
        let parcelsResponse = await BaseService.getAll<IParcel>('/Parcels')
        if (response.ok && response.data && lettersBagsResponse.ok && lettersBagsResponse.data &&
            parcelsBagsResponse.ok && parcelsBagsResponse.data && parcelsResponse.ok && parcelsResponse.data) {
            setShipment(response.data);
            // setShipment({...shipment, flightDate: new Date(response.data.flightDate)});
            setParcelsBags(parcelsBagsResponse.data);
            setLettersBags(lettersBagsResponse.data);
            setParcels(parcelsResponse.data);
            setPageStatus({ pageStatus: EPageStatus.OK, statusCode: 0 });
        } else {
            setPageStatus({ pageStatus: EPageStatus.Error, statusCode: response.statusCode });
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    function handleAirport(e: React.ChangeEvent<HTMLSelectElement>) {
        // if (e.target.value === 'TLL') {
        //     setShipment({ ...shipment, airport: EAirPort.TLL })
        // }
        // if (e.target.value === 'RIX') {
        //     setShipment({ ...shipment, airport: EAirPort.RIX })
        // }
        // if (e.target.value === 'HEL') {
        //     setShipment({ ...shipment, airport: EAirPort.HEL })
        // }
        if (e.target.value === 'TLL') {
            setShipment({ ...shipment, airport: 0 })
        }
        if (e.target.value === 'RIX') {
            setShipment({ ...shipment, airport: 1 })
        }
        if (e.target.value === 'HEL') {
            setShipment({ ...shipment, airport: 2 })
        }
    }

    function handleFinalized() {
        // SHOULD CHECK THAT FLIGHT DATE IS NOT IN THE PAST, IF IN PAST THROW MESSAGE (YOU NEED TO REBOOK TO A FLIGHT THAT IS NOT IN THE PAST etc)
        // SHOULD CHECK THAT THERE ARE NO EMPTY BAGS AND ATLEAST 1 BAG ON THE SHIPMENT
        console.log(shipment)
        if (shipment.flightDate !== null && shipment.flightDate <= new Date()) {
            setAlertMessage("The flight is in the past. You need to book a new flight to finalize this shipment!");
        }
        if (lettersBags.filter(lettersBag => lettersBag.shipmentNumber == shipment.shipmentNumber && lettersBag.countOfLetters > 0).length == 0 ||
            parcelsBags.filter(parcelsBag => parcelsBag.shipmentNumber == shipment.shipmentNumber).length == 0) {
            setAlertMessage("The shipment must contain atleast 1 bag of letters with letters and 1 bag of parcels with parcels!")
        }
        if (parcelsBags.filter(parcelsBag => parcelsBag.shipmentNumber == shipment.shipmentNumber &&
            parcels.filter(parcel => parcel.bagNumber == parcelsBag.bagNumber).length > 0).length !=
            parcelsBags.filter(parcelsBag => parcelsBag.shipmentNumber == shipment.shipmentNumber).length) {
            setAlertMessage("Shipment cannot be finalized with empty parcels bags")
        }
        if (shipment.flightDate !== null && shipment.flightDate > new Date()) {
            setShipment({ ...shipment, finalized: !shipment.finalized })
        }
    }

    return (
        <>
            {pageStatus.pageStatus == EPageStatus.OK ?
                <>
                    <div>
                        <Title title={"Edit Shipment " + id} />
                    </div>

                    <Alert show={alertMessage !== ''} message={alertMessage} alertClass={EAlertClass.Danger} />
                    <Alert show={successMessage !== ''} message={successMessage} alertClass={EAlertClass.Success} />

                    <div className="float-end">
                        <DeleteRounded onClick={(e) => deleteClicked(e.nativeEvent)} className="hover-element" fontSize="large" htmlColor="red" />
                    </div>

                    <table className="table">
                        <tbody>
                            <>
                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="ShipmentNumber">Shipment Number</label>
                                    <input className="form-control" disabled={true} type="text" id="ShipmentNumber" maxLength={10} name="ShipmentNumber"
                                        value={shipment.shipmentNumber} onChange={name => setShipment({ ...shipment, shipmentNumber: name.target.value })} />
                                </tr>

                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="Shipment_FlightNumber">Flight Number</label>
                                    <input className="form-control" type="text" id="Shipment_FlightNumber" maxLength={6} name="Shipment.FlightNumber" placeholder="FlightNumber LLNNNN"
                                        value={shipment.flightNumber} onChange={name => setShipment({ ...shipment, flightNumber: name.target.value })} />
                                </tr>

                                <tr className="form-group mb-2">
                                    <label className="control-label" htmlFor="Shipment_FlightDate">Flight Date </label>
                                    <DatePicker
                                        selected={new Date(shipment.flightDate.toString())}
                                        onChange={e => setShipment({ ...shipment, flightDate: new Date(e!.toString()) })}
                                        showTimeSelect
                                        timeIntervals={1}
                                        dateFormat="dd/MM/yyyy HH:mm"
                                        timeFormat="HH:mm"
                                        className="form-control"
                                        placeholderText="Select flight date and time"
                                    />
                                </tr>

                                <tr className="form-group mb-2">
                                    <td>
                                        <label className="control-label" htmlFor="Shipment_Airport">Airport</label>
                                        <select className="form-control" id="Shipment_Airport" name="Shipment.Airport"
                                            onChange={e => handleAirport(e)}
                                            defaultValue={id ?? <option value="">--- Please select ---</option>}>
                                            {Object.keys(EAirPort).filter(airport => isNaN(Number(airport))).map(value => <option value={value} key={value}>{value}</option>)}
                                        </select>
                                    </td>
                                </tr>

                                <tr className="mb-2 mt-2">
                                    <label htmlFor="Input_Finalized">Finalized</label>
                                    <input type="checkbox" checked={shipment.finalized}
                                        onChange={(e) => handleFinalized()}
                                        className="form-check-input" id="Input_Finalized" name="Input.Finalized" />
                                </tr>

                                <tr className="form-group mt-3 mb-3">
                                    <td>
                                        <button onClick={(e) => editClicked(e.nativeEvent)} type="submit" className="btn btn-primary">Edit</button>
                                    </td>
                                </tr>

                                <tr className="form-control mb-3">
                                    <td>
                                        <SubTitle subTitle={"Letters Bags: " + lettersBags.filter(lettersBag => lettersBag.shipmentNumber == shipment.shipmentNumber).length} />
                                        <Link to={"/lettersBags/create/" + id}>Add new Letters Bag</Link>
                                    </td>
                                    {lettersBags.filter(lettersBag => lettersBag.shipmentNumber == shipment.shipmentNumber).map(lettersBag =>
                                        <>
                                            <td className="form-control hover-element mb-2" onClick={e => navigate("/lettersBags/edit/" + lettersBag.bagNumber)}>
                                                <LettersBagRowDisplay lettersBag={lettersBag} key={lettersBag.bagNumber} />
                                            </td>
                                        </>
                                    )}
                                </tr>

                                <tr className="form-control mb-3">
                                    <td>
                                        <SubTitle subTitle={"Parcels Bags: " + parcelsBags.filter(parcelsBag => parcelsBag.shipmentNumber == shipment.shipmentNumber).length} />
                                        <Link to={"/parcelsBags/create/" + id}>Add new Parcels Bag</Link>
                                    </td>
                                    {parcelsBags.filter(parcelsBag => parcelsBag.shipmentNumber == shipment.shipmentNumber).map(parcelsBag =>
                                        <>
                                            <td className="form-control hover-element mb-2" onClick={e => navigate("/parcelsBags/edit/" + parcelsBag.bagNumber)}>
                                                <ParcelsBagRowDisplay parcelsBag={parcelsBag} parcels={parcels.filter(parcel => parcel.bagNumber == parcelsBag.bagNumber)} key={parcelsBag.bagNumber} />
                                            </td>
                                        </>
                                    )}
                                </tr>
                            </>
                        </tbody>
                    </table>
                </>
                :
                <Loader {...pageStatus} />}
        </>
    )
}

export default ShipmentEdit;