const DetailsTableItem = (props: { title: string, item: string }) => {
    return (
        <>
            <dt className="col-sm-2">
                {props.title}
            </dt>
            <dd className="col-sm-10">
                {props.item}
            </dd>
        </>
    )
};

export default DetailsTableItem;