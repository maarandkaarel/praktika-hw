const Footer = () => {
    return (
        <footer className="border-top footer text-muted">
            <div className="container">
                &copy; 2021 - PostOffice Application
            </div>
        </footer>
    )
}

export default Footer;