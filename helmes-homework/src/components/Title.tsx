const Title = (props: { title: string }) => {
    return (
        <div className="mt-3 mb-3">
            <h1>{props.title}</h1>
        </div>
    )
}

export default Title;