import { EPageStatus } from "../types/EPageStatus";

const Loader = (props: { pageStatus: EPageStatus, statusCode: number}) => {
    if (props.pageStatus === EPageStatus.Loading){
        return (
            <>
                <div className="loader"></div>
                <div className="alert alert-primary" role="alert" text-align="center" vertical-align="middle" line-height="90px">LOADING...</div>
            </>
        )
    }
    if (props.pageStatus === EPageStatus.Error){
        return <div className="alert alert-danger" role="alert">ERROR... {props.statusCode}</div>
    }
    return <></>
}

export default Loader;