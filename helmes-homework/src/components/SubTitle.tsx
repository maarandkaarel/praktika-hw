const SubTitle = (props: { subTitle: string }) => {
    return (
        <h4>{props.subTitle}</h4>
        // can add some design
    );
}
export default SubTitle;