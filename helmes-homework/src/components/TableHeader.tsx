const TableHeader = (props: {headers: string[]}) => {
    return (
        <thead>
            <tr>
                {props.headers.map(function(headers, index){
                    return <th key={index}>{headers}</th>
                })}
                <th></th>
            </tr>
        </thead>
    )
}

export default TableHeader;