const SubTitle2 = (props: { subTitle: string }) => {
    return (
        <h6>{props.subTitle}</h6>
    );
}
export default SubTitle2;