const ColItem = (props: { item: string | number | boolean | Date }) => {
    return (
        <>
            <td>{props.item.toString()}</td>
        </>
    );
}

export default ColItem;