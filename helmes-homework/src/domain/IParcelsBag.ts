import { IParcel } from "./IParcel";
import { IShipment } from "./IShipment";

export interface IParcelsBag {
    bagNumber: string,
    shipmentNumber: string,
    shipment: IShipment | null
}

export interface IParcelsBagAdd {
    shipmentNumber: string,
    shipment: IShipment | null
}