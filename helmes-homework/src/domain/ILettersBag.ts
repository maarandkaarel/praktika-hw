import { IShipment } from "./IShipment";

export interface ILettersBag {
    bagNumber: string,
    countOfLetters: number,
    weight: number,
    price: number,
    shipmentNumber: string,
    shipment: IShipment | null
}

export interface ILettersBagAdd {
    countOfLetters: number,
    weight: number,
    price: number,
    shipmentNumber: string,
    shipment: IShipment | null
}