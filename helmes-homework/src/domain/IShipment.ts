import { EAirPort } from "../types/EAirport";
import { IParcelsBag } from "./IParcelsBag";

export interface IShipment {
    shipmentNumber: string,
    airport: number,
    flightNumber: string,
    flightDate: Date,
    finalized: boolean
}

export interface IShipmentAdd {
    airport: number,
    // airport: EAirPort | null,
    flightNumber: string,
    flightDate: Date,
    finalized: boolean
}