import { IParcelsBag } from "./IParcelsBag";

export interface IParcel {
    parcelNumber: string,
    recipientName: string,
    destinationCountry: string,
    weight: number,
    price: number,
    bagNumber: string,
    parcelsBag: IParcelsBag | null
}

export interface IParcelAdd {
    recipientName: string,
    destinationCountry: string,
    weight: number,
    price: number,
    bagNumber: string,
    parcelsBag: IParcelsBag | null
}