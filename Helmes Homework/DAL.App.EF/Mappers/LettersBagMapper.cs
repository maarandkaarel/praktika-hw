﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class LettersBagMapper : BaseMapper<DAL.App.DTO.LettersBag, Domain.App.LettersBag>, IBaseMapper<DAL.App.DTO.LettersBag, Domain.App.LettersBag>
    {
        public LettersBagMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}