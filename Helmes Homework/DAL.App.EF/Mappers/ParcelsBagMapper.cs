﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class ParcelsBagMapper : BaseMapper<DAL.App.DTO.ParcelsBag, Domain.App.ParcelsBag>, IBaseMapper<DAL.App.DTO.ParcelsBag, Domain.App.ParcelsBag>
    {
        public ParcelsBagMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}