﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class ShipmentMapper : BaseMapper<DAL.App.DTO.Shipment, Domain.App.Shipment>, IBaseMapper<DAL.App.DTO.Shipment, Domain.App.Shipment>
    {
        public ShipmentMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}