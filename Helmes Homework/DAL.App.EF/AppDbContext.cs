﻿using System;
using System.Linq;
using Domain.App;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    public class AppDbContext : DbContext
    {
        public DbSet<Parcel> Parcels { get; set; } = default!;

        public DbSet<LettersBag> LettersBags { get; set; } = default!;

        public DbSet<ParcelsBag> ParcelsBags { get; set; } = default!;

        public DbSet<Shipment> Shipments { get; set; } = default!;

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Parcel>().HasIndex(b => b.ParcelNumber).IsUnique();
            /*
            builder.Entity<Parcel>().Property(e => e.Id).ValueGeneratedOnAdd();
            */
            
            builder.Entity<LettersBag>().HasIndex(b => b.BagNumber).IsUnique();
            /*
            builder.Entity<LettersBag>().Property(e => e.Id).ValueGeneratedOnAdd();
            */
            
            builder.Entity<ParcelsBag>().HasIndex(b => b.BagNumber).IsUnique();
            /*
            builder.Entity<ParcelsBag>().Property(e => e.Id).ValueGeneratedOnAdd();
            */
            
            builder.Entity<Shipment>().HasIndex(b => b.ShipmentNumber).IsUnique();
            /*
            builder.Entity<Shipment>().Property(e => e.Id).ValueGeneratedOnAdd();
            */

            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

        }
    }
}