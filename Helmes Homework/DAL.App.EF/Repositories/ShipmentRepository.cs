﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ShipmentRepository : BaseRepository<DAL.App.DTO.Shipment, Domain.App.Shipment, AppDbContext>, IShipmentRepository
    {
        public ShipmentRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ShipmentMapper(mapper))
        {
        }

        public override async Task<IEnumerable<Shipment>> GetAllAsync(bool noTracking = true)
        {
            var query = CreateQuery();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            
            var resQuery = query
                .Select(x => Mapper.Map(x));

            var res = await resQuery.ToListAsync();

            return res!;
        }

        public override async Task<Shipment?> FirstOrDefaultAsync(string id, bool noTracking = true)
        {
            var query = CreateQuery();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            
            var res = Mapper.Map(await query.FirstOrDefaultAsync(m => m.Id == id));

            return res;
        }
    }
}