﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class LettersBagRepository : BaseRepository<DAL.App.DTO.LettersBag, Domain.App.LettersBag, AppDbContext>, ILettersBagRepository
    {
        public LettersBagRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new LettersBagMapper(mapper))
        {
        }

        public override async Task<IEnumerable<LettersBag>> GetAllAsync(bool noTracking = true)
        {
            var query = CreateQuery(noTracking);
            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            var resQuery = query
                .Include(s => s.Shipment)
                .Select(x => Mapper.Map(x));

            var res = await resQuery.ToListAsync();

            return res!;
        }

        public override async Task<LettersBag?> FirstOrDefaultAsync(string id, bool noTracking = true)
        {
            var query = CreateQuery();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            query = query.Include(s => s.Shipment);

            var res = Mapper.Map(await query.FirstOrDefaultAsync(m => m.Id == id));

            return res;
        }
    }
}