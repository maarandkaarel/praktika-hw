﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ParcelsBagRepository : BaseRepository<DAL.App.DTO.ParcelsBag, Domain.App.ParcelsBag, AppDbContext>, IParcelsBagRepository
    {
        public ParcelsBagRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ParcelsBagMapper(mapper))
        {
        }

        public override async Task<IEnumerable<ParcelsBag>> GetAllAsync(bool noTracking = true)
        {
            var query = CreateQuery();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            
            var resQuery = query
                .Include(s => s.Shipment)
                .Select(x => Mapper.Map(x));

            var res = await resQuery.ToListAsync();

            return res!;
        }

        public override async Task<ParcelsBag?> FirstOrDefaultAsync(string id, bool noTracking = true)
        {
            var query = CreateQuery();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            
            query.Include(s => s.Shipment);

            var res = Mapper.Map(await query.FirstOrDefaultAsync(m => m.Id == id));

            return res;
        }
    }
}