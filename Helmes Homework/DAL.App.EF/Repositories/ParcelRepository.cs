﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ParcelRepository : BaseRepository<DAL.App.DTO.Parcel, Domain.App.Parcel, AppDbContext>, IParcelRepository
    {
        public ParcelRepository(AppDbContext dbContext, IMapper mapper) : base(dbContext, new ParcelMapper(mapper))
        {
        }

        public override async Task<IEnumerable<DAL.App.DTO.Parcel>> GetAllAsync(bool noTracking = true)
        {
            var query = CreateQuery();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }
            
            var resQuery = query
                .Select(x => Mapper.Map(x));

            var res = await resQuery.ToListAsync();

            return res!;
        }

        public override async Task<Parcel?> FirstOrDefaultAsync(string id, bool noTracking = true)
        {
            var query = CreateQuery();

            if (noTracking)
            {
                query.AsNoTracking();
            }
            
            var res = Mapper.Map(await query.FirstOrDefaultAsync(m => m.Id == id));

            return res;
        }
    }
}