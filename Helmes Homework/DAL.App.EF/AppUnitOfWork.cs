﻿using AutoMapper;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Repositories;
using DAL.Base.EF;

namespace DAL.App.EF
{
    public class AppUnitOfWork : BaseUnitOfWork<AppDbContext>, IAppUnitOfWork
    {
        protected IMapper Mapper;
        
        public AppUnitOfWork(AppDbContext uowDbContext, IMapper mapper) : base(uowDbContext)
        {
            Mapper = mapper;
        }

        public IParcelRepository Parcels =>
            GetRepository(() => new ParcelRepository(UowDbContext, Mapper));
        
        public ILettersBagRepository LettersBags =>
            GetRepository(() => new LettersBagRepository(UowDbContext, Mapper));

        public IParcelsBagRepository ParcelsBags =>
            GetRepository(() => new ParcelsBagRepository(UowDbContext, Mapper));

        public IShipmentRepository Shipments =>
            GetRepository(() => new ShipmentRepository(UowDbContext, Mapper));
    }
}