using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1.Mappers;
using PublicApi.DTO.v1;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ParcelsController : ControllerBase
    {
        //private readonly AppDbContext _context;
        private readonly IAppBLL _bll;

        private readonly IMapper Mapper;

        private ParcelMapper _parcelMapper;

        public ParcelsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            Mapper = mapper;
            _parcelMapper = new ParcelMapper(Mapper);
        }

        // GET: api/Parcels
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<PublicApi.DTO.v1.Parcel>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<PublicApi.DTO.v1.Parcel>>> GetParcels()
        {
            var bllParcels = await _bll.Parcels.GetAllAsync();

            var res = bllParcels.Select(x => _parcelMapper.Map(x));

            return Ok(res);
        }

        // GET: api/Parcels/5
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PublicApi.DTO.v1.Parcel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PublicApi.DTO.v1.Parcel>> GetParcel(string id)
        {
            var parcel = await _bll.Parcels.FirstOrDefaultAsync(id);

            if (parcel == null)
            {
                return NotFound();
            }

            return _parcelMapper.Map(parcel)!;
        }

        // PUT: api/Parcels/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParcel(string id, PublicApi.DTO.v1.Parcel parcel)
        {
            if (parcel.ParcelNumber != id)
            {
                return BadRequest();
            }
            
            var bllParcel = _parcelMapper.Map(parcel!);
            bllParcel!.Id = parcel.ParcelNumber;
            _bll.Parcels.Update(bllParcel);
            
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Parcels
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PublicApi.DTO.v1.Parcel>> PostParcel(ParcelAdd parcel)
        {
            var bllParcel = PublicApi.DTO.v1.Mappers.ParcelMapper.MapToBll(parcel);
            /*bllParcel.BagNumber = KeyGen.BagUniqueKey();
            var addedParcel = _bll.Parcels.Add(bllParcel);*/

            var addedParcel = _bll.Parcels.PostParcelWithUniqueId(bllParcel);

            await _bll.SaveChangesAsync();

            var returnParcel = _parcelMapper.Map(addedParcel);

            return CreatedAtAction("GetParcel",
                new
                {
                    version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0",
                    id = returnParcel!.ParcelNumber
                },
                returnParcel);
        }

        // DELETE: api/Parcels/5
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteParcel(string id)
        {
            var parcel = await _bll.Parcels.FirstOrDefaultAsync(id);
            if (parcel == null)
            {
                return NotFound();
            }

            _bll.Parcels.Remove(parcel);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}