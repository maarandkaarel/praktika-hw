using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ShipmentsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        private readonly IMapper Mapper;

        private ShipmentMapper _shipmentMapper;

        public ShipmentsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            Mapper = mapper;
            _shipmentMapper = new ShipmentMapper(Mapper);
        }

        // GET: api/Shipments
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<PublicApi.DTO.v1.Shipment>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<PublicApi.DTO.v1.Shipment>>> GetShipments()
        {
            var bllShipments = await _bll.Shipments.GetAllAsync();

            var res = bllShipments.Select(x => _shipmentMapper.Map(x));

            return Ok(res);
        }

        // GET: api/Shipments/5
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PublicApi.DTO.v1.Shipment), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PublicApi.DTO.v1.Shipment>> GetShipment(string id)
        {
            var shipment = await _bll.Shipments.FirstOrDefaultAsync(id);

            if (shipment == null)
            {
                return NotFound();
            }

            return _shipmentMapper.Map(shipment)!;
        }

        // PUT: api/Shipments/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutShipment(string id, PublicApi.DTO.v1.Shipment shipment)
        {
            if (shipment.ShipmentNumber != id)
            {
                return BadRequest();
            }
            
            var bllShipment = _shipmentMapper.Map(shipment!);
            bllShipment!.Id = shipment.ShipmentNumber;
            _bll.Shipments.Update(bllShipment);
            
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Shipments
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PublicApi.DTO.v1.Shipment>> PostShipment(ShipmentAdd shipment)
        {
            var bllShipment = PublicApi.DTO.v1.Mappers.ShipmentMapper.MapToBll(shipment);
            /*bllShipment.ShipmentNumber = KeyGen.ShipmentUniqueKey();
            var addedShipment = _bll.Shipments.Add(bllShipment);*/
            var addedShipment = _bll.Shipments.PostShipmentWithUniqueId(bllShipment);

            await _bll.SaveChangesAsync();

            var returnShipment = _shipmentMapper.Map(addedShipment);

            return CreatedAtAction("GetShipment",
                new
                {
                    version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0",
                    id = returnShipment!.ShipmentNumber
                },
                returnShipment);
        }

        // DELETE: api/Shipments/5
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteShipment(string id)
        {
            var shipment = await _bll.Shipments.FirstOrDefaultAsync(id);
            if (shipment == null)
            {
                return NotFound();
            }

            _bll.Shipments.Remove(shipment);
            await _bll.SaveChangesAsync();
            
            return NoContent();
        }

    }
}