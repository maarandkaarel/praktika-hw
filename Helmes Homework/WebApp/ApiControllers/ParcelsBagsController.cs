using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ParcelsBagsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        private readonly IMapper Mapper;

        private ParcelsBagMapper _parcelsBagMapper;

        public ParcelsBagsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            Mapper = mapper;
            _parcelsBagMapper = new ParcelsBagMapper(Mapper);
        }

        // GET: api/ParcelsBags
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<PublicApi.DTO.v1.ParcelsBag>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<PublicApi.DTO.v1.ParcelsBag>>> GetParcelsBags()
        {
            var bllParcelsBags = await _bll.ParcelsBags.GetAllAsync();

            var res = bllParcelsBags.Select(x => _parcelsBagMapper.Map(x));

            return Ok(res);
        }

        // GET: api/ParcelsBags/5
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PublicApi.DTO.v1.ParcelsBag), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PublicApi.DTO.v1.ParcelsBag>> GetParcelsBag(string id)
        {
            var parcelsBag = await _bll.ParcelsBags.FirstOrDefaultAsync(id);

            if (parcelsBag == null)
            {
                return NotFound();
            }

            return _parcelsBagMapper.Map(parcelsBag)!;
        }

        // PUT: api/ParcelsBags/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParcelsBag(string id, PublicApi.DTO.v1.ParcelsBag parcelsBag)
        {
            if (parcelsBag!.BagNumber != id)
            {
                return BadRequest();
            }

            var bllParcelsBag = _parcelsBagMapper.Map(parcelsBag!);
            bllParcelsBag!.Id = parcelsBag.BagNumber;
            _bll.ParcelsBags.Update(bllParcelsBag);
            
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/ParcelsBags
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PublicApi.DTO.v1.ParcelsBag>> PostParcelsBag(ParcelsBagAdd parcelsBag)
        {
            var bllParcelsBag = PublicApi.DTO.v1.Mappers.ParcelsBagMapper.MapToBll(parcelsBag);
            /*bllParcelsBag.BagNumber = KeyGen.BagUniqueKey();
            var addedParcelsBag = _bll.ParcelsBags.Add(bllParcelsBag);*/

            var addedParcelsBag = _bll.ParcelsBags.PostParcelsBagWithUniqueId(bllParcelsBag);
            
            await _bll.SaveChangesAsync();


            var returnParcelsBag = _parcelsBagMapper.Map(addedParcelsBag);

            return CreatedAtAction("GetParcelsBag", new
                {
                    version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0",
                    id = returnParcelsBag!.BagNumber
                },
                returnParcelsBag);
        }

        // DELETE: api/ParcelsBags/5
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteParcelsBag(string id)
        {
            var parcelsBag = await _bll.ParcelsBags.FirstOrDefaultAsync(id);
            if (parcelsBag == null)
            {
                return NotFound();
            }

            _bll.ParcelsBags.Remove(parcelsBag);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}