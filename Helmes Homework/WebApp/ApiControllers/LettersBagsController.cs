using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PublicApi.DTO.v1;
using PublicApi.DTO.v1.Mappers;


namespace WebApp.ApiControllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class LettersBagsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        private readonly IMapper Mapper;

        private LettersBagMapper _lettersBagsMapper;

        public LettersBagsController(IAppBLL bll, IMapper mapper)
        {
            _bll = bll;
            Mapper = mapper;
            _lettersBagsMapper = new LettersBagMapper(Mapper);
        }

        // GET: api/LettersBags
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<PublicApi.DTO.v1.LettersBag>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<PublicApi.DTO.v1.LettersBag>>> GetLettersBags()
        {
            var bllLettersBags = await _bll.LettersBags.GetAllAsync();

            var res = bllLettersBags.Select(x => _lettersBagsMapper.Map(x));

            return Ok(res);
        }

        // GET: api/LettersBags/5
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(PublicApi.DTO.v1.LettersBag), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<PublicApi.DTO.v1.LettersBag>> GetLettersBag(string id)
        {
            var lettersBag = await _bll.LettersBags.FirstOrDefaultAsync(id);

            if (lettersBag == null)
            {
                return NotFound();
            }

            return _lettersBagsMapper.Map(lettersBag)!;
        }

        // PUT: api/LettersBags/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLettersBag(string id, PublicApi.DTO.v1.LettersBag lettersBag)
        {
            if (id != lettersBag.BagNumber)
            {
                return BadRequest();
            }

            var bllLettersBag = _lettersBagsMapper.Map(lettersBag!);
            bllLettersBag!.Id = lettersBag.BagNumber;
            _bll.LettersBags.Update(bllLettersBag);
            
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/LettersBags
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PublicApi.DTO.v1.LettersBag>> PostLettersBag(LettersBagAdd lettersBag)
        {
            var bllLettersBag = PublicApi.DTO.v1.Mappers.LettersBagMapper.MapToBll(lettersBag);
            /*bllLettersBag.BagNumber = KeyGen.BagUniqueKey();
            var addedLettersBag = _bll.LettersBags.Add(bllLettersBag);*/
            var addedLettersBag = _bll.LettersBags.PostLettersBagWithUniqueId(bllLettersBag);

            await _bll.SaveChangesAsync();

            var returnLettersBag = _lettersBagsMapper.Map(addedLettersBag);

            return CreatedAtAction("GetLettersBag", new
            {
                version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0",
                id = returnLettersBag!.BagNumber
            }, returnLettersBag);
        }

        // DELETE: api/LettersBags/5
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteLettersBag(string id)
        {
            var lettersBag = await _bll.LettersBags.FirstOrDefaultAsync(id);
            if (lettersBag == null)
            {
                return NotFound();
            }

            _bll.LettersBags.Remove(lettersBag);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}