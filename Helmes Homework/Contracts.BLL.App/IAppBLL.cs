﻿using System;
using Contracts.BLL.App.Services;
using Contracts.BLL.Base;

namespace Contracts.BLL.App
{
    public interface IAppBLL : IBaseBLL
    {
        IParcelService Parcels { get; }
        
        ILettersBagService LettersBags { get; }
        
        IParcelsBagService ParcelsBags { get; }
        
        IShipmentService Shipments { get; }
    }
}