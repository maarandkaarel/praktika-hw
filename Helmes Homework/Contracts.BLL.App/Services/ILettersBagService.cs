﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using BLLAppDTO = BLL.App.DTO;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.BLL.App.Services
{
    public interface ILettersBagService : IBaseEntityService<BLLAppDTO.LettersBag, DALAppDTO.LettersBag>, ILettersBagRepositoryCustom<BLLAppDTO.LettersBag>
    {
        BLLAppDTO.LettersBag PostLettersBagWithUniqueId(BLLAppDTO.LettersBag entity);
    }
}