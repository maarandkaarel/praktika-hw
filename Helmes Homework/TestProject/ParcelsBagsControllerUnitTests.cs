﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.BLL.App;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using PublicApi.DTO.v1;
using UniqueKeyGenerator;
using WebApp.ApiControllers;
using Xunit;
using Xunit.Abstractions;
using ParcelsBag = BLL.App.DTO.ParcelsBag;

namespace TestProject
{
    public class ParcelsBagsControllerUnitTests
    {
        private readonly ITestOutputHelper _output;
        private readonly Mock<IMapper> _mapperStub = new();
        private readonly Mock<IAppBLL> _repoStub = new();
        private readonly Random _random = new();


        public ParcelsBagsControllerUnitTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async Task GetParcelsBag_WithNotExistingParcelsBag_ReturnsNotFound()
        {
            // ARRANGE
            _repoStub.Setup(repo => repo.ParcelsBags.FirstOrDefaultAsync(It.IsAny<string>(), true))
                .ReturnsAsync((ParcelsBag?)null);

            var controller = new ParcelsBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.GetParcelsBag(It.IsAny<string>());

            //ASSERT
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task GetParcelsBag_WithExistingParcelsBag_ReturnsExpectedParcelsBag()
        {
            ParcelsBag expectedParcelsBag = CreateRandomParcelsBag();

            // ARRANGE
            _repoStub.Setup(repo => repo.ParcelsBags.FirstOrDefaultAsync(expectedParcelsBag.BagNumber, true))
                .ReturnsAsync(expectedParcelsBag);

            var controller = new ParcelsBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.GetParcelsBag(expectedParcelsBag.BagNumber);

            //ASSERT
            result.Value.Should().BeEquivalentTo(expectedParcelsBag);
        }

        [Fact]
        public async Task GetParcelsBags_WithExistingParcelsBags_ReturnsAllParcelsBags()
        {
            // ARRANGE
            var count = 5;
            var expectedParcelsBags = GetParcelsBags(count);
            _repoStub.Setup(repo => repo.ParcelsBags.GetAllAsync(true))
                .ReturnsAsync(expectedParcelsBags);

            var controller = new ParcelsBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var actualShipments = await controller.GetParcelsBags();

            //ASSERT
            actualShipments.Should()
                .BeEquivalentTo(expectedParcelsBags, options => options.ComparingByMembers<ParcelsBag>());
        }

        [Fact]
        public async Task CreateParcelsBag_WithParcelsBagToCreate_ReturnsCreatedParcelsBag()
        {
            // ARRANGE
            var parcelsBagToCreate = new ParcelsBagAdd()
            {
                ShipmentNumber = KeyGen.ShipmentUniqueKey(),
                Shipment = null
            };
            var controller = new ParcelsBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.PostParcelsBag(parcelsBagToCreate);
            
            //ASSERT
            var createdParcelsBag = (result.Result as CreatedAtActionResult)!.Value as ParcelsBag;
            parcelsBagToCreate.Should().BeEquivalentTo(
                createdParcelsBag,
                options => options.ComparingByMembers<ParcelsBag>().ExcludingMissingMembers()
            );

            createdParcelsBag!.ShipmentNumber.Should().NotBeEmpty();
        }

        [Fact]
        public async Task UpdateParcelsBag_WithExistingParcelsBag_ReturnsNoContent()
        {
            // ARRANGE
            ParcelsBag existingParcelsBag = CreateRandomParcelsBag();
            _repoStub.Setup(repo => repo.ParcelsBags.FirstOrDefaultAsync(existingParcelsBag.BagNumber, true))
                .ReturnsAsync(existingParcelsBag);

            var bagNumber = existingParcelsBag.BagNumber;
            var parcelsBagToUpdate = new PublicApi.DTO.v1.ParcelsBag()
            {
                BagNumber = bagNumber,
                ShipmentNumber = KeyGen.ShipmentUniqueKey(),
                Shipment = null
            };
            
            var controller = new ParcelsBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.PutParcelsBag(bagNumber, parcelsBagToUpdate);

            //ASSERT
            result.Should().BeOfType<NoContentResult>();
        }

        [Fact]
        public async Task DeleteParcelsBag_WithExistingParcelsBag_ReturnsNoContent()
        {
            // ARRANGE
            ParcelsBag existingParcelsBag = CreateRandomParcelsBag();
            _repoStub.Setup(repo => repo.ParcelsBags.FirstOrDefaultAsync(existingParcelsBag.BagNumber, true))
                .ReturnsAsync(existingParcelsBag);

            var controller = new ParcelsBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.DeleteParcelsBag(existingParcelsBag.BagNumber);

            //ASSERT
            result.Should().BeOfType<NoContentResult>();
        }
        private ParcelsBag CreateRandomParcelsBag()
        {
            var uniqueKey = KeyGen.BagUniqueKey();
            return new ParcelsBag()
            {
                Id = uniqueKey,
                BagNumber = uniqueKey,
                ShipmentNumber = KeyGen.ShipmentUniqueKey(),
                Shipment = null
            };
        }

        private List<ParcelsBag> GetParcelsBags(int count)
        {
            var parcelsBags = new List<ParcelsBag>();
            for (int i = 0; i < count; i++)
            {
                parcelsBags.Add(CreateRandomParcelsBag());
            }

            return parcelsBags;
        }
    }
}