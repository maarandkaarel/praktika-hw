using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.BLL.App;
using Domain.App.Enums;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using PublicApi.DTO.v1;
using PublicApi.DTO.v1.Mappers;
using UniqueKeyGenerator;
using WebApp.ApiControllers;
using Xunit;
using Xunit.Abstractions;
using Shipment = BLL.App.DTO.Shipment;

namespace TestProject
{
    public class ShipmentsControllerUnitTests
    {
        private readonly ITestOutputHelper _output;
        private readonly Mock<IMapper> _mapperStub = new();
        private readonly Mock<IAppBLL> _repoStub = new();
        private readonly Random _random = new();


        public ShipmentsControllerUnitTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async Task GetShipment_WithNotExistingShipment_ReturnsNotFound()
        {
            // ARRANGE
            _repoStub.Setup(repo => repo.Shipments.FirstOrDefaultAsync(It.IsAny<string>(), true))
                .ReturnsAsync((Shipment?)null);

            var controller = new ShipmentsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.GetShipment(It.IsAny<string>());

            //ASSERT
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task GetShipment_WithExistingShipment_ReturnsExpectedShipment()
        {
            Shipment expectedShipment = CreateRandomShipment();

            // ARRANGE
            _repoStub.Setup(repo => repo.Shipments.FirstOrDefaultAsync(expectedShipment.ShipmentNumber, true))
                .ReturnsAsync(expectedShipment);

            var controller = new ShipmentsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.GetShipment(expectedShipment.ShipmentNumber);

            //ASSERT
            result.Value.Should().BeEquivalentTo(expectedShipment);
        }

        [Fact]
        public async Task GetShipments_WithExistingShipments_ReturnsAllShipments()
        {
            // ARRANGE
            var count = 5;
            var expectedShipments = GetShipments(count);
            _repoStub.Setup(repo => repo.Shipments.GetAllAsync(true))
                .ReturnsAsync(expectedShipments);

            var controller = new ShipmentsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var actualShipments = await controller.GetShipments();

            //ASSERT
            (actualShipments.Result as OkObjectResult)!.Value.Should()
                .BeEquivalentTo(expectedShipments, options => options.ComparingByMembers<Shipment>());
        }

        [Fact]
        public async Task CreateShipment_WithShipmentToCreate_ReturnsCreatedShipment()
        {
            // ARRANGE
            var shipmentToCreate = new ShipmentAdd()
            {
                Airport = Airports.HEL,
                Finalized = false,
                FlightDate = DateTime.Today.AddDays(5),
                FlightNumber = "xx9999"
            };
            var controller = new ShipmentsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.PostShipment(shipmentToCreate);
            
            //ASSERT
            var createdShipment = (result.Result as CreatedAtActionResult)!.Value as Shipment;
            shipmentToCreate.Should().BeEquivalentTo(
                createdShipment,
                options => options.ComparingByMembers<Shipment>().ExcludingMissingMembers()
            );

            createdShipment!.ShipmentNumber.Should().NotBeEmpty();
        }

        [Fact]
        public async Task UpdateShipment_WithExistingShipment_ReturnsNoContent()
        {
            // ARRANGE
            Shipment existingShipment = CreateRandomShipment();
            _repoStub.Setup(repo => repo.Shipments.FirstOrDefaultAsync(existingShipment.ShipmentNumber, true))
                .ReturnsAsync(existingShipment);

            var shipmentNumber = existingShipment.ShipmentNumber;
            var shipmentToUpdate = new PublicApi.DTO.v1.Shipment()
            {
                Airport = Airports.RIX,
                Finalized = false,
                FlightDate = DateTime.Today.AddDays(_random.Next(100)),
                FlightNumber = "ff5555",
                ShipmentNumber = shipmentNumber
            };
            
            var controller = new ShipmentsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.PutShipment(shipmentNumber, shipmentToUpdate);

            //ASSERT
            result.Should().BeOfType<NoContentResult>();
        }

        [Fact]
        public async Task DeleteShipment_WithExistingShipment_ReturnsNoContent()
        {
            // ARRANGE
            Shipment existingShipment = CreateRandomShipment();
            _repoStub.Setup(repo => repo.Shipments.FirstOrDefaultAsync(existingShipment.ShipmentNumber, true))
                .ReturnsAsync(existingShipment);

            var controller = new ShipmentsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.DeleteShipment(existingShipment.ShipmentNumber);

            //ASSERT
            result.Should().BeOfType<NoContentResult>();
        }
        private Shipment CreateRandomShipment()
        {
            var uniqueKey = KeyGen.ShipmentUniqueKey();
            return new Shipment()
            {
                Id = uniqueKey,
                ShipmentNumber = uniqueKey,
                Airport = Airports.HEL,
                FlightDate = DateTime.Today.AddTicks(_random.Next()),
                FlightNumber = "xx" + _random.Next(1000, 9999),
                Finalized = false
            };
        }

        private IQueryable<Shipment> GetShipments(int count)
        {
            var shipments = new List<Shipment>();
            for (int i = 0; i < count; i++)
            {
                shipments.Add(CreateRandomShipment());
            }

            return shipments.AsQueryable();
        }
    }
}