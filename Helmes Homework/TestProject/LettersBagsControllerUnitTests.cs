﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.BLL.App;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using PublicApi.DTO.v1;
using UniqueKeyGenerator;
using WebApp.ApiControllers;
using Xunit;
using Xunit.Abstractions;
using LettersBag = BLL.App.DTO.LettersBag;

namespace TestProject
{
    public class LettersBagsControllerUnitTests
    {
        private readonly ITestOutputHelper _output;
        private readonly Mock<IMapper> _mapperStub = new();
        private readonly Mock<IAppBLL> _repoStub = new();
        private readonly Random _random = new();


        public LettersBagsControllerUnitTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async Task GetLettersBag_WithNotExistingLettersBag_ReturnsNotFound()
        {
            // ARRANGE
            _repoStub.Setup(repo => repo.LettersBags.FirstOrDefaultAsync(It.IsAny<string>(), true))
                .ReturnsAsync((LettersBag?)null);

            var controller = new LettersBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.GetLettersBag(It.IsAny<string>());

            //ASSERT
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task GetLettersBag_WithExistingLettersBag_ReturnsExpectedLettersBag()
        {
            LettersBag expectedLettersBag = CreateRandomLettersBag();

            // ARRANGE
            _repoStub.Setup(repo => repo.LettersBags.FirstOrDefaultAsync(expectedLettersBag.BagNumber, true))
                .ReturnsAsync(expectedLettersBag);

            var controller = new LettersBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.GetLettersBag(expectedLettersBag.BagNumber);

            //ASSERT
            result.Value.Should().BeEquivalentTo(expectedLettersBag);
        }

        [Fact]
        public async Task GetLettersBags_WithExistingLettersBags_ReturnsAllLettersBags()
        {
            // ARRANGE
            var count = 5;
            var expectedLettersBags = GetLettersBags(count);
            _repoStub.Setup(repo => repo.LettersBags.GetAllAsync(true))
                .ReturnsAsync(expectedLettersBags);

            var controller = new LettersBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var actualShipments = await controller.GetLettersBags();

            //ASSERT
            actualShipments.Should()
                .BeEquivalentTo(expectedLettersBags, options => options.ComparingByMembers<LettersBag>());
        }

        [Fact]
        public async Task CreateLettersBag_WithLettersBagToCreate_ReturnsCreatedLettersBag()
        {
            // ARRANGE
            var lettersBagToCreate = new LettersBagAdd()
            {
                ShipmentNumber = KeyGen.ShipmentUniqueKey(),
                Shipment = null,
                CountOfLetters = _random.Next(1, 1000),
                Price = _random.Next(0, 10000),
                Weight = _random.Next(0, 1000)
            };
            var controller = new LettersBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.PostLettersBag(lettersBagToCreate);

            //ASSERT
            var createdLettersBag = (result.Result as CreatedAtActionResult)!.Value as LettersBag;
            lettersBagToCreate.Should().BeEquivalentTo(
                createdLettersBag,
                options => options.ComparingByMembers<LettersBag>().ExcludingMissingMembers()
            );

            createdLettersBag!.ShipmentNumber.Should().NotBeEmpty();
        }

        [Fact]
        public async Task UpdateLettersBag_WithExistingLettersBag_ReturnsNoContent()
        {
            // ARRANGE
            LettersBag existingLettersBag = CreateRandomLettersBag();
            _repoStub.Setup(repo => repo.LettersBags.FirstOrDefaultAsync(existingLettersBag.BagNumber, true))
                .ReturnsAsync(existingLettersBag);

            var bagNumber = existingLettersBag.BagNumber;
            var lettersBagToUpdate = new PublicApi.DTO.v1.LettersBag()
            {
                BagNumber = bagNumber,
                ShipmentNumber = KeyGen.ShipmentUniqueKey(),
                Shipment = null,
                CountOfLetters = _random.Next(1, 1000),
                Price = _random.Next(0, 10000),
                Weight = _random.Next(0, 1000)
            };

            var controller = new LettersBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.PutLettersBag(bagNumber, lettersBagToUpdate);

            //ASSERT
            result.Should().BeOfType<NoContentResult>();
        }

        [Fact]
        public async Task DeleteLettersBag_WithExistingLettersBag_ReturnsNoContent()
        {
            // ARRANGE
            LettersBag existingLettersBag = CreateRandomLettersBag();
            _repoStub.Setup(repo => repo.LettersBags.FirstOrDefaultAsync(existingLettersBag.BagNumber, true))
                .ReturnsAsync(existingLettersBag);

            var controller = new LettersBagsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.DeleteLettersBag(existingLettersBag.BagNumber);

            //ASSERT
            result.Should().BeOfType<NoContentResult>();
        }

        private LettersBag CreateRandomLettersBag()
        {
            var uniqueKey = KeyGen.BagUniqueKey();
            return new LettersBag()
            {
                Id = uniqueKey,
                BagNumber = uniqueKey,
                ShipmentNumber = KeyGen.ShipmentUniqueKey(),
                Shipment = null,
                CountOfLetters = _random.Next(1, 1000),
                Price = _random.Next(0, 10000),
                Weight = _random.Next(0, 1000)
            };
        }

        private List<LettersBag> GetLettersBags(int count)
        {
            var lettersBags = new List<LettersBag>();
            for (int i = 0; i < count; i++)
            {
                lettersBags.Add(CreateRandomLettersBag());
            }

            return lettersBags;
        }
    }
}