﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.BLL.App;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using PublicApi.DTO.v1;
using UniqueKeyGenerator;
using WebApp.ApiControllers;
using Xunit;
using Xunit.Abstractions;
using Parcel = BLL.App.DTO.Parcel;

namespace TestProject
{
    public class ParcelsControllerUnitTests
    {
        private readonly ITestOutputHelper _output;
        private readonly Mock<IMapper> _mapperStub = new();
        private readonly Mock<IAppBLL> _repoStub = new();
        private readonly Random _random = new();


        public ParcelsControllerUnitTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async Task GetParcel_WithNotExistingParcel_ReturnsNotFound()
        {
            // ARRANGE
            _repoStub.Setup(repo => repo.Parcels.FirstOrDefaultAsync(It.IsAny<string>(), true))
                .ReturnsAsync((Parcel?)null);

            var controller = new ParcelsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.GetParcel(It.IsAny<string>());

            //ASSERT
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task GetParcel_WithExistingParcel_ReturnsExpectedParcel()
        {
            Parcel expectedParcel = CreateRandomParcel();

            // ARRANGE
            _repoStub.Setup(repo => repo.Parcels.FirstOrDefaultAsync(expectedParcel.ParcelNumber, true))
                .ReturnsAsync(expectedParcel);

            var controller = new ParcelsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.GetParcel(expectedParcel.ParcelNumber);

            //ASSERT
            result.Value.Should().BeEquivalentTo(expectedParcel);
        }

        [Fact]
        public async Task GetParcels_WithExistingParcels_ReturnsAllParcels()
        {
            // ARRANGE
            var count = 5;
            var expectedParcels = GetParcels(count);
            _repoStub.Setup(repo => repo.Parcels.GetAllAsync(true))
                .ReturnsAsync(expectedParcels);

            var controller = new ParcelsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var actualParcels = await controller.GetParcels();

            //ASSERT
            actualParcels.Should()
                .BeEquivalentTo(expectedParcels, options => options.ComparingByMembers<Parcel>());
        }

        [Fact]
        public async Task CreateParcel_WithParcelToCreate_ReturnsCreatedParcel()
        {
            // ARRANGE
            var parcelToCreate = new ParcelAdd()
            {
                BagNumber = KeyGen.BagUniqueKey(),
                Price = _random.Next(1, 10000),
                Weight = _random.Next(1, 10000),
                DestinationCountry = "EE",
                RecipientName = "Someone random",
                ParcelsBag = null
            };
            var controller = new ParcelsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.PostParcel(parcelToCreate);
            
            //ASSERT
            var createdParcel = (result.Result as CreatedAtActionResult)!.Value as Parcel;
            parcelToCreate.Should().BeEquivalentTo(
                createdParcel,
                options => options.ComparingByMembers<Parcel>().ExcludingMissingMembers()
            );

            createdParcel!.ParcelNumber.Should().NotBeEmpty();
        }

        [Fact]
        public async Task UpdateParcel_WithExistingParcel_ReturnsNoContent()
        {
            // ARRANGE
            Parcel existingParcel = CreateRandomParcel();
            _repoStub.Setup(repo => repo.Parcels.FirstOrDefaultAsync(existingParcel.ParcelNumber, true))
                .ReturnsAsync(existingParcel);

            var parcelNumber = existingParcel.ParcelNumber;
            var parcelToUpdate = new PublicApi.DTO.v1.Parcel()
            {
                ParcelNumber = parcelNumber,
                BagNumber = KeyGen.BagUniqueKey(),
                Price = _random.Next(1, 10000),
                Weight = _random.Next(1, 10000),
                DestinationCountry = "EE",
                RecipientName = "Someone random",
                ParcelsBag = null
            };
            
            var controller = new ParcelsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.PutParcel(parcelNumber, parcelToUpdate);

            //ASSERT
            result.Should().BeOfType<NoContentResult>();
        }

        [Fact]
        public async Task DeleteParcel_WithExistingParcel_ReturnsNoContent()
        {
            // ARRANGE
            Parcel existingParcel = CreateRandomParcel();
            _repoStub.Setup(repo => repo.Parcels.FirstOrDefaultAsync(existingParcel.ParcelNumber, true))
                .ReturnsAsync(existingParcel);

            var controller = new ParcelsController(_repoStub.Object, _mapperStub.Object);

            // ACT
            var result = await controller.DeleteParcel(existingParcel.ParcelNumber);

            //ASSERT
            result.Should().BeOfType<NoContentResult>();
        }
        private Parcel CreateRandomParcel()
        {
            var uniqueKey = KeyGen.ParcelUniqueKey();
            return new Parcel()
            {
                Id = uniqueKey,
                ParcelNumber = uniqueKey,
                BagNumber = KeyGen.BagUniqueKey(),
                Price = _random.Next(1, 10000),
                Weight = _random.Next(1, 10000),
                DestinationCountry = "EE",
                RecipientName = "Someone random",
                ParcelsBag = null
            };
        }

        private List<Parcel> GetParcels(int count)
        {
            var parcels = new List<Parcel>();
            for (int i = 0; i < count; i++)
            {
                parcels.Add(CreateRandomParcel());
            }

            return parcels;
        }
    }
}