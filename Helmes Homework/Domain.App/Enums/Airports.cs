﻿namespace Domain.App.Enums
{
    public enum Airports
    {
        TLL,
        RIX,
        HEL
    }
}