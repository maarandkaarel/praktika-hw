﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Base;

namespace Domain.App
{
    public class Parcel : DomainEntityId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(10)]
        [RegularExpression(@"[a-zA-Z]{2}[0-9]{6}[a-zA-Z]{2}")]
        public string ParcelNumber { get; set; } = default!;
        // Parcel number string	Format “LLNNNNNNLL”, where L – letter, N – digit
        // Must be unique within entire database

        [MaxLength(100)] 
        public string RecipientName { get; set; } = default!;
        // Max length 100 chars

        [MaxLength(2)] 
        public string DestinationCountry { get; set; } = default!;
        // Alpha 2 Code - 2-letters code, e.g. “EE”, “LV”, “FI” MAX length 2
        
        [RegularExpression(@"^(?=.*\d)\d*(?:\.\d{0,3})?$")]
        [Range(0, 999999999999999.999)]
        public float Weight { get; set; } 
        // Max 3 decimals after comma
        
        [RegularExpression(@"^(?=.*\d)\d*(?:\.\d{0,2})?$")]
        [Range(0, 9999999999999999.99)]
        public float Price { get; set; } 
        // Max 2 decimals after comma

        public string? BagNumber { get; set; } 
        
        public ParcelsBag? ParcelsBag { get; set; }
    }
}