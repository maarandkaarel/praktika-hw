﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.App.Enums;
using Domain.Base;

namespace Domain.App
{
    public class ShipmentWithCollections : DomainEntityId
    {
        [RegularExpression(@"\w{3}\b-\w{6}")] 
        public string ShipmentNumber { get; set; } = default!;
        // Shipment number	string	Format “XXX-XXXXXX”, where X – letter or digit
        // Must be unique within entire database

        public Airports Airport { get; set; } = default!;

        [RegularExpression(@"[a-zA-Z]{2}[0-9]{4}")]
        public string FlightNumber { get; set; } = default!;
        // Flight number	string	Format “LLNNNN”, where L – letter, N – digit
        
        public DateTime FlightDate { get; set; }
        // Can’t be in past by the moment of finalizing shipment
        
        public bool Finalized { get; set; }

        public ICollection<LettersBag>? BagsWithLetters { get; set; }
        // List of bags	collection	Can’t be empty by the moment of finalizing shipment
        
        public ICollection<ParcelsBag>? BagsWithParcels { get; set; }
        // List of bags	collection	Can’t be empty by the moment of finalizing shipment
    }
}