﻿using AutoMapper;

namespace PublicApi.DTO.v1.MappingProfiles
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<BLL.App.DTO.Parcel, ParcelAdd>().ReverseMap();
            CreateMap<BLL.App.DTO.Parcel, Parcel>().ReverseMap();
            CreateMap<BLL.App.DTO.LettersBag, LettersBagAdd>().ReverseMap();
            CreateMap<BLL.App.DTO.LettersBag, LettersBag>().ReverseMap();
            CreateMap<BLL.App.DTO.ParcelsBag, ParcelsBagAdd>().ReverseMap();
            CreateMap<BLL.App.DTO.ParcelsBag, ParcelsBag>().ReverseMap();
            CreateMap<BLL.App.DTO.Shipment, ShipmentAdd>().ReverseMap();
            CreateMap<BLL.App.DTO.Shipment, Shipment>().ReverseMap();
        }
    }
}