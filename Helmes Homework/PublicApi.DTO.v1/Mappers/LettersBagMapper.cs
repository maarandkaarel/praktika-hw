﻿using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class LettersBagMapper : BaseMapper<PublicApi.DTO.v1.LettersBag, BLL.App.DTO.LettersBag>
    {
        public LettersBagMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLL.App.DTO.LettersBag MapToBll(LettersBagAdd lettersBagAdd)
        {
            var res = new BLL.App.DTO.LettersBag()
            {
                CountOfLetters = lettersBagAdd.CountOfLetters,
                Weight = lettersBagAdd.Weight,
                Price = lettersBagAdd.Price,
                ShipmentNumber = lettersBagAdd.ShipmentNumber
            };
            
            return res;
        }
    }
}