﻿using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class ShipmentMapper : BaseMapper<PublicApi.DTO.v1.Shipment, BLL.App.DTO.Shipment>
    {
        public ShipmentMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLL.App.DTO.Shipment MapToBll(ShipmentAdd shipmentAdd)
        {
            var res = new BLL.App.DTO.Shipment()
            {
                Airport = shipmentAdd.Airport,
                FlightNumber = shipmentAdd.FlightNumber,
                FlightDate = shipmentAdd.FlightDate,
                Finalized = shipmentAdd.Finalized
            };

            return res;
        }
    }
}