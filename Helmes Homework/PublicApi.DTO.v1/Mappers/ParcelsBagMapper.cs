﻿using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class ParcelsBagMapper: BaseMapper<PublicApi.DTO.v1.ParcelsBag, BLL.App.DTO.ParcelsBag>
    {
        public ParcelsBagMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLL.App.DTO.ParcelsBag MapToBll(ParcelsBagAdd parcelsBagAdd)
        {
            var res = new BLL.App.DTO.ParcelsBag()
            {
                ShipmentNumber = parcelsBagAdd.ShipmentNumber
            };

            return res;
        }
    }
}