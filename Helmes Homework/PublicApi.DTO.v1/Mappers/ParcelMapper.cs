﻿using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class ParcelMapper : BaseMapper<PublicApi.DTO.v1.Parcel, BLL.App.DTO.Parcel>
    {
        public ParcelMapper(IMapper mapper) : base(mapper)
        {
        }

        public static BLL.App.DTO.Parcel MapToBll(ParcelAdd parcelAdd)
        {
            var res = new BLL.App.DTO.Parcel()
            {
                RecipientName = parcelAdd.RecipientName,
                DestinationCountry = parcelAdd.DestinationCountry,
                BagNumber = parcelAdd.BagNumber,
                Weight = parcelAdd.Weight,
                Price = parcelAdd.Price
            };
            
            return res;
        }
    }
}