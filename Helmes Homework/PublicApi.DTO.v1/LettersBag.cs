﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1
{
    public class LettersBagAdd
    {
        [Range(1, int.MaxValue, ErrorMessage = "Count can't be 0!")]
        public int CountOfLetters { get; set; } // Can't be zero
        
        [RegularExpression(@"^(?=.*\d)\d*(?:\.\d{0,3})?$")]
        [Range(0, 999999999999999.999)]
        public float Weight { get; set; } // max 3 decimals after comma
        
        [RegularExpression(@"^(?=.*\d)\d*(?:\.\d{0,2})?$")]
        [Range(0, 9999999999999999.99)]
        public float Price { get; set; } // max 2 decimals after comma

        public string ShipmentNumber { get; set; } = default!;
        
        public Shipment? Shipment { get; set; }
    }
    public class LettersBag
    {
        [MaxLength(15)]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Letters and Numbers allowed!")]
        public string BagNumber { get; set; } = default!;
        //Max length 15 characters, no special symbols allowed
        //Must be unique within entire database. Bags with parcels and letters share the same numbers,
        //so the same bag number can’t be assigned to bag with parcels and bag with letters
        
        [Range(1, int.MaxValue, ErrorMessage = "Count can't be 0!")]
        public int CountOfLetters { get; set; } // Can't be zero
        
        [RegularExpression(@"^(?=.*\d)\d*(?:\.\d{0,3})?$")]
        [Range(0, 999999999999999.999)]
        public float Weight { get; set; } // max 3 decimals after comma
        
        [RegularExpression(@"^(?=.*\d)\d*(?:\.\d{0,2})?$")]
        [Range(0, 9999999999999999.99)]
        public float Price { get; set; } // max 2 decimals after comma

        public string ShipmentNumber { get; set; } = default!;
        
        public Shipment? Shipment { get; set; }
    }
}