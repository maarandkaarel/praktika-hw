﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace BLL.App.Mappers
{
    public class ParcelsBagMapper : BaseMapper<BLL.App.DTO.ParcelsBag, DAL.App.DTO.ParcelsBag>,
        IBaseMapper<BLL.App.DTO.ParcelsBag, DAL.App.DTO.ParcelsBag>
    {
        public ParcelsBagMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}