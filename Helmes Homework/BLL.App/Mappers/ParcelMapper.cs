﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace BLL.App.Mappers
{
    public class ParcelMapper : BaseMapper<BLL.App.DTO.Parcel, DAL.App.DTO.Parcel>,
        IBaseMapper<BLL.App.DTO.Parcel, DAL.App.DTO.Parcel>
    {
        public ParcelMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}