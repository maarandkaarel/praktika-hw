﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace BLL.App.Mappers
{
    public class ShipmentMapper : BaseMapper<BLL.App.DTO.Shipment, DAL.App.DTO.Shipment>,
        IBaseMapper<BLL.App.DTO.Shipment, DAL.App.DTO.Shipment>
    {
        public ShipmentMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}