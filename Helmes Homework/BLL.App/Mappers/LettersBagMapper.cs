﻿using AutoMapper;
using Contracts.DAL.Base.Mappers;

namespace BLL.App.Mappers
{
    public class LettersBagMapper : BaseMapper<BLL.App.DTO.LettersBag, DAL.App.DTO.LettersBag>,
        IBaseMapper<BLL.App.DTO.LettersBag, DAL.App.DTO.LettersBag>
    {
        public LettersBagMapper(IMapper mapper) : base(mapper)
        {
        }
    }
}