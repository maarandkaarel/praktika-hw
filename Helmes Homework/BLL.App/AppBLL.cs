﻿using System;
using AutoMapper;
using BLL.App.Services;
using BLL.Base;
using Contracts.BLL.App;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App
{
    public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
    {
        protected IMapper Mapper;
        
        public AppBLL(IAppUnitOfWork uow, IMapper mapper) : base(uow)
        {
            Mapper = mapper;
        }

        public IParcelService Parcels =>
            GetService<IParcelService>(() => new ParcelService(Uow, Uow.Parcels, Mapper));

        public ILettersBagService LettersBags =>
            GetService<ILettersBagService>(() => new LettersBagService(Uow, Uow.LettersBags, Mapper));

        public IParcelsBagService ParcelsBags =>
            GetService<IParcelsBagService>(() => new ParcelsBagService(Uow, Uow.ParcelsBags, Mapper));

        public IShipmentService Shipments =>
            GetService<IShipmentService>(() => new ShipmentService(Uow, Uow.Shipments, Mapper));
    }
}