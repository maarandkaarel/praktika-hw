﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base.Mappers;
using UniqueKeyGenerator;

namespace BLL.App.Services
{
    public class ShipmentService :
        BaseEntityService<IAppUnitOfWork, IShipmentRepository, BLL.App.DTO.Shipment, DAL.App.DTO.Shipment>,
        IShipmentService
    {
        public ShipmentService(IAppUnitOfWork serviceUow, IShipmentRepository serviceRepository,
            IMapper mapper) : base(serviceUow, serviceRepository, new ShipmentMapper(mapper))
        {
        }

        public Shipment PostShipmentWithUniqueId(BLL.App.DTO.Shipment entity)
        {
            var uniqueKey = KeyGen.ShipmentUniqueKey();
            entity.ShipmentNumber = uniqueKey;
            entity.Id = uniqueKey;
            ServiceRepository.Add(Mapper.Map(entity)!);
            return entity;
        }
    }
}