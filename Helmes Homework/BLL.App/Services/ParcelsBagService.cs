﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base.Mappers;
using UniqueKeyGenerator;

namespace BLL.App.Services
{
    public class ParcelsBagService :
        BaseEntityService<IAppUnitOfWork, IParcelsBagRepository, BLL.App.DTO.ParcelsBag, DAL.App.DTO.ParcelsBag>,
        IParcelsBagService
    {
        public ParcelsBagService(IAppUnitOfWork serviceUow, IParcelsBagRepository serviceRepository,
            IMapper mapper) : base(serviceUow, serviceRepository, new ParcelsBagMapper(mapper))
        {
        }

        public ParcelsBag PostParcelsBagWithUniqueId(BLL.App.DTO.ParcelsBag entity)
        {
            var uniqueKey = KeyGen.BagUniqueKey();
            entity.BagNumber = uniqueKey;
            entity.Id = uniqueKey;
            ServiceRepository.Add(Mapper.Map(entity)!);
            return entity;
        }
    }
}