﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base.Mappers;
using UniqueKeyGenerator;

namespace BLL.App.Services
{
    public class ParcelService :
        BaseEntityService<IAppUnitOfWork, IParcelRepository, BLL.App.DTO.Parcel, DAL.App.DTO.Parcel>,
        IParcelService
    {
        public ParcelService(IAppUnitOfWork serviceUow, IParcelRepository serviceRepository,
            IMapper mapper) : base(serviceUow, serviceRepository, new ParcelMapper(mapper))
        {
        }

        public Parcel PostParcelWithUniqueId(BLL.App.DTO.Parcel entity)
        {
            var uniqueKey = KeyGen.ParcelUniqueKey();
            entity.ParcelNumber = uniqueKey;
            entity.Id = uniqueKey;
            ServiceUow.Parcels.Add(Mapper.Map(entity)!);
            return entity;
            
        }
    }
}