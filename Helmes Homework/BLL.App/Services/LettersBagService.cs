﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base.Mappers;
using UniqueKeyGenerator;

namespace BLL.App.Services
{
    public class LettersBagService :
        BaseEntityService<IAppUnitOfWork, ILettersBagRepository, BLL.App.DTO.LettersBag, DAL.App.DTO.LettersBag>,
        ILettersBagService
    {
        public LettersBagService(IAppUnitOfWork serviceUow, ILettersBagRepository serviceRepository,
            IMapper mapper) : base(serviceUow, serviceRepository, new LettersBagMapper(mapper))
        {
        }

        public LettersBag PostLettersBagWithUniqueId(BLL.App.DTO.LettersBag entity)
        {
            var uniqueKey = KeyGen.BagUniqueKey();
            entity.BagNumber = uniqueKey;
            entity.Id = uniqueKey;
            ServiceRepository.Add(Mapper.Map(entity)!);
            return entity;
        }
    }
}