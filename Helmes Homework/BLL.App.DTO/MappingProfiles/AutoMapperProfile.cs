﻿using AutoMapper;

namespace BLL.App.DTO.MappingProfiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Parcel, DAL.App.DTO.Parcel>().ReverseMap();
            CreateMap<LettersBag, DAL.App.DTO.LettersBag>().ReverseMap();
            CreateMap<ParcelsBag, DAL.App.DTO.ParcelsBag>().ReverseMap();
            CreateMap<Shipment, DAL.App.DTO.Shipment>().ReverseMap();
        }
    }
}