﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Base;

namespace DAL.App.DTO
{
    public class ParcelsBag : DomainEntityId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [MaxLength(15)]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Only Letters and Numbers allowed!")]
        public string BagNumber { get; set; } = default!;
        //Max length 15 characters, no special symbols allowed
        //Must be unique within entire database. Bags with parcels and letters share the same numbers,
        //so the same bag number can’t be assigned to bag with parcels and bag with letters

        public string ShipmentNumber { get; set; } = default!;
        
        public Shipment? Shipment { get; set; }
    }
}