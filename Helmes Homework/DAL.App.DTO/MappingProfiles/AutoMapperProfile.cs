﻿using AutoMapper;

namespace DAL.App.DTO.MappingProfiles
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Parcel, Domain.App.Parcel>().ReverseMap();
            CreateMap<LettersBag, Domain.App.LettersBag>().ReverseMap();
            CreateMap<ParcelsBag, Domain.App.ParcelsBag>().ReverseMap();
            CreateMap<Shipment, Domain.App.Shipment>().ReverseMap();
        }
    }
}