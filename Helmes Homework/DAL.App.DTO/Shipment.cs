﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.App.Enums;
using Domain.Base;

namespace DAL.App.DTO
{
    public class Shipment : DomainEntityId
    {
        [RegularExpression(@"[a-zA-Z0-9]{3}[-][a-zA-Z0-9]{6}")]
        public string ShipmentNumber { get; set; } = default!;
        // Shipment number	string	Format “XXX-XXXXXX”, where X – letter or digit
        // Must be unique within entire database

        public Airports Airport { get; set; } = default!;

        [RegularExpression(@"[a-zA-Z]{2}[0-9]{4}")]
        public string FlightNumber { get; set; } = default!;
        // Flight number	string	Format “LLNNNN”, where L – letter, N – digit

        public DateTime FlightDate { get; set; }
        // Can’t be in past by the moment of finalizing shipment

        public bool Finalized { get; set; }
    }
}