﻿using System;
using System.Linq;

namespace UniqueKeyGenerator
{
    // All keys must be unique within entire database
    public class KeyGen
    {
        private static Random random = new Random();
        private static string alphabetWithNumbers = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        private static string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        private static string numbers = "0123456789";

        public static string ParcelUniqueKey()
        {
            // Format “LLNNNNNNLL”, where L – letter, N – digit
            string res = "";

            string firstPart =
                new (Enumerable.Repeat(alphabet, 2).Select(s => s[random.Next(s.Length)]).ToArray());

            string middlePart =
                new (Enumerable.Repeat(numbers, 6).Select(s => s[random.Next(s.Length)]).ToArray());

            string lastPart =
                new (Enumerable.Repeat(alphabet, 2).Select(s => s[random.Next(s.Length)]).ToArray());

            res = firstPart + middlePart + lastPart;
            return res;
        }

        public static string BagUniqueKey()
        {
            // Max length 15 characters, no special symbols allowed
            string res = new string(Enumerable.Repeat(alphabetWithNumbers, 15)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return res;
        }

        public static string ShipmentUniqueKey()
        {
            // Format “XXX-XXXXXX”, where X – letter or digit
            string res = new string(Enumerable.Repeat(alphabetWithNumbers, 9).Select(s => s[random.Next(s.Length)]).ToArray());
            res = res.Substring(0, 3) + '-' + res.Substring(3);
            return res;
        }
    }
}