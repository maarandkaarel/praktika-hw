﻿using System;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork
    {
        IParcelRepository Parcels { get; }
        
        ILettersBagRepository LettersBags { get; }
        
        IParcelsBagRepository ParcelsBags { get; }
        
        IShipmentRepository Shipments { get; }
    }
}