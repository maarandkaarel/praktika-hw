﻿using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ILettersBagRepository : IBaseRepository<DALAppDTO.LettersBag>, ILettersBagRepositoryCustom<DALAppDTO.LettersBag>
    {
    }

    public interface ILettersBagRepositoryCustom<TEntity>
    {
        
    }
}