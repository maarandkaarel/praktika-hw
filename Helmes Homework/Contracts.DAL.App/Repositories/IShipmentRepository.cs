﻿using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IShipmentRepository : IBaseRepository<DALAppDTO.Shipment>, IShipmentRepositoryCustom<DALAppDTO.Shipment>
    {
    }

    public interface IShipmentRepositoryCustom<TEntity>
    {
    }
}