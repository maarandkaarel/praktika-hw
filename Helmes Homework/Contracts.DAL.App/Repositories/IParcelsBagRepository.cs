﻿using Contracts.DAL.Base.Repositories;
using DALAppDTO = DAL.App.DTO;

namespace Contracts.DAL.App.Repositories

{
    public interface IParcelsBagRepository : IBaseRepository<DALAppDTO.ParcelsBag>, IParcelRepositoryCustom<DALAppDTO.ParcelsBag>
    {
    }

    public interface IParcelsBagRepositoryCustom<TEntity>
    {
    }
}